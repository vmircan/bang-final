#include "Logging.h"

#include <cstdio>
#include <ctime>

Logger::Logger(std::ostream & logTo) :
	logTo(logTo) {}

void Logger::log(const std::string & message, LogType type)
{
	switch (type)
	{
	case Logger::LogType::Info:
		logTo << "--Info-- ";
		break;
	case Logger::LogType::Warning:
		logTo << "--Warning-- ";
		break;
	case Logger::LogType::Error:
		logTo << "--Error-- ";
		break;
	}
	logTo << message << " ";
	std::time_t currentTime = std::time(0);
	std::tm* localTime = localtime(&currentTime);
	char timeStamp[100];
	sprintf_s(timeStamp, "%d %d %d %d:%d:%d", localTime->tm_year + 1900, localTime->tm_mon, localTime->tm_mday, localTime->tm_hour, localTime->tm_min, localTime->tm_sec);
	logTo << timeStamp << std::endl;
}
