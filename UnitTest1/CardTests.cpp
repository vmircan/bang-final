#include "stdafx.h"
#include "CppUnitTest.h"
#include "Bang.h"
#include "Weapon.h"
#include "DrawingCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangTests
{		
	TEST_CLASS(CardTests)
	{
	public:
		
		TEST_METHOD(BasicCardConstructor)
		{
			Bang card("card", CardColor::Clubs, 5);
			Assert::IsTrue(!strcmp(card.getName().c_str(), "card"));
			Assert::IsTrue(card.getColor() == CardColor::Clubs);
			Assert::IsTrue(card.getNumber() == 5);
		}

		TEST_METHOD(WeaponConstructor)
		{
			Weapon weapon("weapon", CardColor::Heart, 11, 5, false);
			Assert::IsTrue(!strcmp(weapon.getName().c_str(), "weapon"));
			Assert::IsTrue(weapon.getColor() == CardColor::Heart);
			Assert::IsTrue(weapon.getNumber() == 11);
			Assert::IsTrue(weapon.getWeaponRange() == 5);
			Assert::IsTrue(weapon.hasUnlimitedBangs() == false);
		}

		TEST_METHOD(DrawingCardConstructor)
		{
			DrawingCard drawCard("Diligenza", CardColor::Clubs, 6, 2);
			Assert::IsTrue(!strcmp(drawCard.getName().c_str(), "Diligenza"));
			Assert::IsTrue(drawCard.getColor() == CardColor::Clubs);
			Assert::IsTrue(drawCard.getNumber() == 6);
			Assert::IsTrue(drawCard.getNumberOfCardsToDraw() == 2);
		}

	};
}