#pragma once
#include"Weapon.h"

Weapon::Weapon(const std::string& name, CardColor color, uint8_t number, uint8_t range, bool unlimitedBangs) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Weapon, CardTargetType::Untargeted),
	range(range), unlimitedBangs(unlimitedBangs) {}

void Weapon::playCard(Player* player, PlayerIteratorRange & range) {
	state = CardState::Played;
	playCard(player, player);
}

void Weapon::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has put " << name << " into play\n";
	player->putWeaponInPlay(this);
	player->setPlayerRange(this->range);
	if (unlimitedBangs)
		player->setUnlimitedBangs(true);
}

uint8_t Weapon::getWeaponRange() const
{
	return range;
}

bool Weapon::hasUnlimitedBangs() const
{
	return unlimitedBangs;
}

void Weapon::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

bool Weapon::canBePlayed(Player * player) const
{
	return true;
}

