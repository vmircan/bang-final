#pragma once
#include"Card.h"

class Indians :
	public Card
{
public:
	Indians(const std::string& name, CardColor color, uint8_t number);
	void playCard(Player* player, PlayerIteratorRange & range) override;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	void playCard(Player* player, Player* targetedPlayer) override;
	bool canBePlayed(Player* player) const override;
};

