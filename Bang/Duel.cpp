#include "Duel.h"

Duel::Duel(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Duel, CardTargetType::Targeted) {}

void Duel::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	Player* targetedPlayer = range.getPlayerTargetChoice();
	playCard(player, targetedPlayer);
}
void Duel::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Duel::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played " << name << " on " << targetedPlayer->getPlayerName() << "\n";
	while ((!targetedPlayer->isComputer() && targetedPlayer->useBang()) ||
		(targetedPlayer->isComputer() && targetedPlayer->decideOnBangUse()))
		std::swap(player, targetedPlayer);
	std::cout << player->getPlayerName() << " has won the duel against " << targetedPlayer->getPlayerName() << "\n";
	bool defended = targetedPlayer->decreaseLifePoints(DamageSource::Duel);
	if (!defended && targetedPlayer->getCharacter() == BangCharacters::ElGringo) {
		if (player->getNumberOfCards()) {
			if (!player->isComputer())
				targetedPlayer->drawCard(player->discardRandomCard());
			else
				targetedPlayer->drawCard(player->decideOnDiscard());
			std::cout << targetedPlayer->getPlayerName() << " drew from " << player->getPlayerName() << "s hand\n";
		}
	}
}

bool Duel::canBePlayed(Player * player) const
{
	return true;
}
