#include "Beer.h"

Beer::Beer(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Beer, CardTargetType::Untargeted) {}

void Beer::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (canBePlayed(player))
		playCard(player, player);
	else {
		std::cout << "Can't play a " << name << " if you are at max life\n";
		state = CardState::Unplayed;
	}
}

void Beer::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Beer::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " played a " << name << "\n";
	player->increaseLifePoints();
}

bool Beer::canBePlayed(Player * player) const
{
	return player->getPlayerLifePoints() < player->getPlayerMaxLifePoints();
}
