#include "Card.h"

Card::Card(const std::string& name, CardColor color, uint8_t number, CardType type,
	BangCard bangCardType, CardTargetType targetType, CardRange range, bool defendableFrom) :
	name(name), color(color), number(number), type(type), targetType(targetType),
	bangCardType(bangCardType), range(range), defendableFrom(defendableFrom) {}

Card::~Card()
{
}


Card::Card(const Card & otherCard) : 
	name(otherCard.name) {}

Card::Card(Card && otherCard) noexcept
{
	std::swap(name, otherCard.name);
}

Card & Card::operator=(const Card & otherCard)
{
	name = otherCard.name;
	return *this;
}

Card & Card::operator=(Card && otherCard) noexcept
{
	if (this != &otherCard)
		std::swap(name, otherCard.name);
	return *this;
}

inline CardType Card::getType() const
{
	return type;
}

CardRange Card::getRange() const
{
	return range;
}

inline CardState Card::getCardState() const
{
	return state;
}

bool Card::isDefendableFrom() const
{
	return defendableFrom;
}

inline CardColor Card::getColor() const
{
	return color;
}

inline uint8_t Card::getNumber() const
{
	return number;
}

inline BangCard Card::getBangCardType() const
{
	return bangCardType;
}

CardTargetType Card::getTargetType() const
{
	return targetType;
}

inline const std::string & Card::getName() const
{
	return name;
}