#include "stdafx.h"
#include "CppUnitTest.h"
#include "BangGame.h"
#include "State.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangTests
{
	TEST_CLASS(BangGameTests)
	{
	public:

		TEST_METHOD(BasicConstructor) {
			BangGame game(5, 0);
			Assert::IsTrue(game.getNumberOfPlayers() == 5);
		}

		TEST_METHOD(Run) {
			BangGame game(5, 0);
			game.run();
			Assert::IsTrue(game.gameState != GameState::NotFinished);
		}

		TEST_METHOD(RefreshDeck) {
			BangGame game(5, 0);
			unsigned deckSize = game.deck.size();
			unsigned discardedCardsSize = game.discardedCards.size();
			game.refreshDeck();
			Assert::IsTrue(game.deck.size() == discardedCardsSize);
		}
	};
}