#pragma once
#include "Player.h"
#include <vector>
#include <functional>

class Player;

struct PlayerIteratorRange {
	virtual Player* getPlayerTargetChoice(std::function<bool(Player*)> lamda = [](Player*) {return false; }) = 0;
	virtual void filterRange(std::function<bool(Player*)> lamda = [](Player*) {return false; }) = 0;
	virtual void for_each(std::function<void(Player*)> lamda) = 0;
	virtual void clear() = 0;
};