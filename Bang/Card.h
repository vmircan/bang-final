#pragma once
#include "BangGameEnums.h"
#include "CardInterface.h"
#include "Player.h"
#include "PlayerIteratorRange.h"

#include <algorithm>
#include <iostream>
#include <string>

using namespace BangGameEnums;

class Card : public CardInterface
{
public:
	Card(const std::string& name, CardColor color, uint8_t number, CardType type, BangCard bangCardType,
		CardTargetType targetType, CardRange range=CardRange::RangeIndependent, bool defendableFrom = false);
	~Card();
	Card(const Card& otherCard);
	Card(Card&& otherCard) noexcept;
	Card& operator= (const Card& otherCard);
	Card& operator= (Card&& otherCard) noexcept;
	inline CardType getType() const;
	inline CardTargetType getTargetType() const;
	inline CardRange getRange() const;
	inline CardState getCardState() const;
	bool isDefendableFrom() const;
	inline CardColor getColor() const;
	inline uint8_t getNumber() const;
	inline BangCard getBangCardType() const override;
	virtual inline const std::string& getName() const;
	virtual void playCard(Player* player, PlayerIteratorRange & range) = 0;
	virtual void playCard(Player* player, Player* targetedPlayer) = 0;
	virtual void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) = 0;
	virtual bool canBePlayed(Player* player) const = 0;
protected:
	std::string name;
	CardState state;
private:
	CardType type;
	BangCard bangCardType;
	CardTargetType targetType;
	CardRange range;
	CardColor color;
	uint8_t number;
	bool defendableFrom;
};

