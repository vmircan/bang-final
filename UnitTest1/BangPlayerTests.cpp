#include "stdafx.h"
#include "CppUnitTest.h"
#include "BangPlayer.h"
#include "Weapon.h"
#include "Missed.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangTests
{
	TEST_CLASS(BangPlayerTests)
	{
	public:

		TEST_METHOD(BangPlayerConstructor)
		{
			BangPlayer player("John Doe", Roles::Renegade, BangCharacters::PaulRegret);
			Assert::IsTrue(!strcmp(player.getPlayerName().c_str(), "John Doe"));
			Assert::IsTrue(player.getRole() == Roles::Renegade);
			Assert::IsTrue(player.getCharacter() == BangCharacters::PaulRegret);
		}
		TEST_METHOD(DecreaseLifePoints)
		{
			BangPlayer player("John Doe", Roles::Renegade, BangCharacters::Jourdonnais);
			uint8_t lifePoints = player.getPlayerLifePoints();
			player.decreaseLifePoints(DamageSource::Bang);
			Assert::IsTrue(lifePoints - 1 == player.getPlayerLifePoints());
		}
		TEST_METHOD(IncreaseLifePoints) {
			BangPlayer player("John Doe", Roles::Renegade, BangCharacters::Jourdonnais);
			uint8_t lifePoints = player.getPlayerLifePoints();
			player.increaseLifePoints();
			Assert::IsFalse(lifePoints != player.getPlayerLifePoints());
		}
		TEST_METHOD(DiscardAllCards) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			player.discardAllCards();
			Assert::IsTrue(!player.getNumberOfActiveCards() && !player.getNumberOfCards());
		}
		TEST_METHOD(DiscardChosenCard) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			unsigned handSize = player.getNumberOfCards();
			player.discardChosenCard();
			Assert::IsTrue(player.getNumberOfCards() <= handSize - 1);
		}
		TEST_METHOD(DiscardRandomCard) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			unsigned handSize = player.getNumberOfCards();
			player.discardRandomCard();
			Assert::IsTrue(player.getNumberOfCards() <= handSize - 1);
		}
		TEST_METHOD(DiscardCard) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			unsigned handSize = player.getNumberOfCards();
			player.discardCard();
			Assert::IsTrue(player.getNumberOfCards() <= handSize - 1);
		}
		TEST_METHOD(UseBang) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			unsigned handSize = player.getNumberOfCards();
			player.useBang();
			Assert::IsTrue(player.getNumberOfCards() <= handSize - 1);
		}
		TEST_METHOD(PutWeaponInPlay) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			Weapon* w = new Weapon("weapon", CardColor::Heart, 12, 2, false);
			player.drawCard(w);
			unsigned handSize = player.getNumberOfCards();
			unsigned activeCardsSize = player.getNumberOfActiveCards();
			player.putWeaponInPlay(w);
			Assert::IsTrue(player.getNumberOfCards() <= handSize && player.getNumberOfActiveCards() <= activeCardsSize);
		}
		TEST_METHOD(ImpossibleDefend) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			Assert::IsFalse(player.defend(5));
		}
		TEST_METHOD(EasyDefend) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			player.setDamageReduction(2);
			Assert::IsTrue(player.decreaseLifePoints(DamageSource::SlabTheKiller));
		}
		TEST_METHOD(EasyDefend2) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::PaulRegret);
			player.setDamageReduction(1);
			Assert::IsTrue(player.decreaseLifePoints(DamageSource::Bang));
		}
		TEST_METHOD(CantPlayMissed) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::ElGringo);
			Missed* missedCard = dynamic_cast<Missed*>(player.playCard());
			Assert::IsNull(missedCard);
		}
		TEST_METHOD(CantUseAnInvalidIndex) {
			BangPlayer player("John Doe", Roles::Sheriff, BangCharacters::KitCarlson);
			Assert::IsTrue(player.getCardChoice() < player.getNumberOfCards());
		}

	};
}