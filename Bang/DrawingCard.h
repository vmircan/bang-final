#pragma once
#include"Card.h"

class DrawingCard :
	public Card
{
public:
	DrawingCard(const std::string& name, CardColor color, uint8_t number, uint8_t numberOfCardsToDraw);
	void playCard(Player* player, PlayerIteratorRange & range) override final;
	uint8_t getNumberOfCardsToDraw() const;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	void playCard(Player* player, Player* targetedPlayer) override;
	bool canBePlayed(Player* player) const override;
private:
	uint8_t numberOfCardsToDraw;
};

