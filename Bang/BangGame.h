#pragma once

#include "BangGameEnums.h"
#include "CircularList.h"
#include "BangPlayer.h"
#include "BangPlayerIteratorRange.h"
#include "CardInterface.h"
#include "Card.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>

using namespace BangGameEnums;


class BangGame
{
public:
	BangGame(unsigned numberOfPlayers, unsigned numberOfComputers);
	~BangGame();

	uint8_t getNumberOfPlayers();
	std::ostream& printDeck(std::ostream& os) const;
	void run();

public:
	BangPlayer* getPlayer(uint8_t index);
	inline BangPlayer& getActivePlayer();
	BangPlayerIteratorRange&
		getPlayersInRange(BangPlayer* player, uint8_t range);
	BangPlayerIteratorRange&
		getPlayersInRange(uint8_t playerIndex, uint8_t range);
	BangPlayerIteratorRange&
		getPlayersInRange();
	inline uint8_t getActivePlayerIndex();
	BangPlayerIteratorRange& getOtherPlayers(BangPlayer* player);
	BangPlayerIteratorRange& getOtherPlayers();
	std::vector<Player*> getOtherPlayersVector() const;
	void distributeRolesAndCharacters();
	std::ostream& displayRolesAndCharacters(std::ostream& os);
	void setRelativeDistances();
	void setAugmentedRelativeDistances();
	void checkForDistanceAugmentations();
	void distributeCards();
	void removePlayer(BangPlayer* player);
	void updatePlayerIndexMap();
	void initializeDeck();
	void refreshDeck();
	void handleKill(BangPlayer* activePlayer, BangPlayer* targetedPlayer=nullptr);
	void displayPlayerDeath(BangPlayer* activePlayer, BangPlayer* targetedPlayer) const;
	void displayPlayerDeath(BangPlayer* killedPlayer) const;
	void handleVictory(GameState gameState);
	void giveCardsFromDeckToPlayer(BangPlayer* player, unsigned numberOfCards);
	void giveCardToPlayer(BangPlayer* player, Card* card);
	void endTurn(BangPlayer* player);
	void drawPhase(BangPlayer* player);
	void luckDrawPhase(BangPlayer* player);
	bool promptPlayerForAlternativeDrawingMechanic(BangPlayer* player);
	bool promptPlayerToDiscardCardsForLife(BangPlayer* player);
	bool promptPlayerForEndOfTurn(BangPlayer* player);
	bool checkForPlayerDeath(BangPlayer* activePlayer);
	bool checkForActivePlayerDeath(BangPlayer* activePlayer);
	bool testForDynamiteExplosion(BangPlayer* activePlayer);
	bool testForGettingOutOfJail(BangPlayer* activePlayer);
	bool testForGettingOutOfJail();
	void checkForBarrels();
	void discardCardsFromDeck(uint8_t numberOfCards);
	void distributeCardsForGeneralStore(uint8_t activePlayerIndex);

	GameState checkGameState();
public:
	uint8_t numberOfPlayers;
	uint8_t numberOfComputers;
	std::vector<BangPlayer*> players;
	std::vector<BangPlayer*> playersCopy;
	std::vector<BangPlayer*> playersCopy2;
	BangPlayerIteratorRange otherPlayers;
	BangPlayerIteratorRange playersInRange;
	std::unordered_map<BangPlayer*, uint8_t> playerIndexMap;
	std::vector<Card*> deck;
	std::vector<Card*> discardedCards;
	std::vector <std::multimap<uint8_t, uint8_t>> relativeDistances;
	CircularList<BangPlayer> table;
	bool sheriff, renegade;
	uint8_t banditCount, deputieCount;
	GameState gameState;
	BangPlayer* activePlayer=nullptr;
	uint8_t activePlayerIndex;
private:
	const unsigned numberOfCards = 80;
	const unsigned cardsPerTurn = 2;
	const unsigned dynamiteDamage = 3;
	bool distanceAugmentations = false;
	bool passDynamiteToPlayer = false;
};

