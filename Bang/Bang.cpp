#include "Bang.h"
#include <iostream>
#include "temp.h"

Bang::Bang(const std::string & name, CardColor color, uint8_t number):
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Bang, CardTargetType::Targeted, CardRange::RangeDependent, true){}

void Bang::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (canBePlayed(player)) {
		Player* targetedPlayer = range.getPlayerTargetChoice();
		playCard(player, targetedPlayer);
	}
	else {
		std::cout << "Can't play another " << name << " this turn\n";
		state = CardState::Unplayed;
	}

}

void Bang::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
	if (!(player->getNumberOfBangsPlayed() < player->getNumberOfBangsPerTurn()))
		range.clear();
}

bool Bang::canBePlayed(Player * player) const
{
	return player->getNumberOfBangsPlayed() < player->getNumberOfBangsPerTurn();
}

void Bang::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	bool defended;
	std::cout << player->getPlayerName() << " has played a " << name << " on: " << targetedPlayer->getPlayerName() << "\n";
	if (player->getCharacter() == BangCharacters::SlabTheKiller)
		defended = targetedPlayer->decreaseLifePoints(DamageSource::SlabTheKiller);
	else
		defended = targetedPlayer->decreaseLifePoints(DamageSource::Bang);
	if (!defended && targetedPlayer->getCharacter() == BangCharacters::ElGringo) {
		if (player->getNumberOfCards()) {
			if (!player->isComputer())
				targetedPlayer->drawCard(player->discardRandomCard());
			else
				targetedPlayer->drawCard(player->decideOnDiscard());
			std::cout << targetedPlayer->getPlayerName() << " drew from " << player->getPlayerName() << "s hand\n";
		}
	}
	player->increaseBangsPlayed();
	player->setNumberOfCardsToBeDiscardedFromDeck(targetedPlayer->getNumberOfCardsToBeDiscardedFromDeck());
	targetedPlayer->setNumberOfCardsToBeDiscardedFromDeck(0);
}
