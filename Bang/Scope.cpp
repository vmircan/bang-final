#include "Scope.h"

Scope::Scope(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Scope, CardTargetType::Untargeted) {}

void Scope::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (canBePlayed(player))
		playCard(player, player);
	else {
		std::cout << "Can't play another " << name << "\n";
		state = CardState::Unplayed;
	}
}

void Scope::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Scope::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << "\n";
	player->setScope(true);
	player->setRecalculateRelativeDistances(true);
}

bool Scope::canBePlayed(Player * player) const
{
	return !player->hasScope();
}
