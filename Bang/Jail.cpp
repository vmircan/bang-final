#include "Jail.h"

Jail::Jail(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Jail, CardTargetType::Targeted) {}

void Jail::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	Player* targetedPlayer = range.getPlayerTargetChoice([](Player* player) {return player->isInJail(); });
	if (targetedPlayer) {
		if (targetedPlayer->getRole() != Roles::Sheriff)
			playCard(player, targetedPlayer);
		else {
			std::cout << "Cant't put the sheriff in jail\n";
			state = CardState::Unplayed;
		}
	}
	else
		state = CardState::Unplayed;
}

void Jail::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
	range.filterRange([](Player* player) {return player->isInJail() || player->getRole() == Roles::Sheriff; });
}

void Jail::playCard(Player * player, Player * targetedPlayer)
{
	targetedPlayer->putInJail();
	targetedPlayer->addToActiveCards(this);
	std::cout << player->getPlayerName() << " has put " << targetedPlayer->getPlayerName() << " in " << name << "\n";
	player->setPlayedJail(true);
}

bool Jail::canBePlayed(Player * player) const
{
	return true;
}
