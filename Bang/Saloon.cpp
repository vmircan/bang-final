#include "Saloon.h"

Saloon::Saloon(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Saloon, CardTargetType::Untargeted) {}

void Saloon::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	bool increasedLifePoints = false;
	range.for_each([&increasedLifePoints](Player * player) {
		if (player->increaseLifePoints())
			increasedLifePoints = true;
	});
	if (player->increaseLifePoints())
		increasedLifePoints = true;;
	if (player->increaseLifePoints())
		increasedLifePoints = true;
	if (!increasedLifePoints)
		state = CardState::Unplayed;
	if (state == CardState::Played)
		std::cout << player->getPlayerName() << " has played a " << name << "\n";
}

void Saloon::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Saloon::playCard(Player * player, Player * targetedPlayer)
{
}

bool Saloon::canBePlayed(Player * player) const
{
	return true;
}
