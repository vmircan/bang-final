#include "Mustang.h"

Mustang::Mustang(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Missed, CardTargetType::Untargeted) {}

void Mustang::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (canBePlayed(player))
		playCard(player, player);
	else {
		std::cout << "Can't play another " << name << "\n";
		state = CardState::Unplayed;
	}
}

void Mustang::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Mustang::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << "\n";
	player->setMustang(true);
	player->setRecalculateRelativeDistances(true);
}

bool Mustang::canBePlayed(Player * player) const
{
	return !player->hasMustang();
}
