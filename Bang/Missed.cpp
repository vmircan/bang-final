#include "Missed.h"

Missed::Missed(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Missed, CardTargetType::Untargeted) {}

void Missed::playCard(Player * player, PlayerIteratorRange & range)
{
	
}

void Missed::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Missed::playCard(Player * player, Player * targetedPlayer)
{
}

bool Missed::canBePlayed(Player * player) const
{
	return false;
}
