#ifdef LOGGING_EXPORTS
#define LOGGING_DLL __declspec(dllexport)
#else
#define LOGGING_DLL __declspec(dllimport)
#endif // LOGGING_EXPORTS

#include <iostream>
#include <string>

class LOGGING_DLL Logger {
public:
	enum class LogType :uint8_t {
		Info,
		Warning,
		Error,
	};
public:
	Logger(std::ostream& logTo);
	void log(const std::string& message, LogType type = LogType::Info);
private:
	std::ostream& logTo;
};