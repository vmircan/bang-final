#pragma once
#include "StateInterface.h"
#include "Player.h"

#include <string>
#include <vector>

class State : public StateInterface
{
public:
	State() = default;
	State(StateInterface* parentState, Player* player, Player* targetedPlayer, CardInterface* play,
		double gCost, double hCost, CardInterface* targetedCard = nullptr);

	double getGCost() const override;
	double getHCost() const override;
	double getFCost() const override;
	static void calculateHCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	static void calculateGCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	static void calculateFCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	static void calculateUnreachableHCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	static void calculateUnreachableGCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	static void calculateUnreachableFCost(State* currentState, PlayerIteratorRange& otherPlayers, PlayerIteratorRange& playersInRange);
	double calculateHCost(CardInterface* card) const override;
	double calculateGCost(CardInterface* card) const override;
	double calculateFCost(CardInterface* card) const override;
	void setGCost(double gCost) override;
	void setHCost(double hCost) override;
	CardInterface* getPlay() const;
	Player* getPlayer() const;
	Player* getTargetedPlayer();
	void setOtherPlayers(PlayerIteratorRange& otherPlayers);
	void setInRangePlayers(PlayerIteratorRange& inRangePlayers);
	const std::vector<Player*>& getOtherPlayers() const;
	const std::vector<Player*>& getInRangePlayers() const;
	void addOtherPlayer(Player* otherPlayer);
	void addInRangePlayer(Player* inRangePlayer);
	CardInterface* getTargetedCard() const;
	void setTargetedCard(CardInterface* targetedCard);
private:
	StateInterface* parentState;
	CardInterface* play;
	Player* player;
	Player* targetedPlayer;
	CardInterface* targetedCard;
	std::vector<Player*> otherPlayers;
	std::vector<Player*> inRangePlayers;
	double gCost;
	double hCost;
};

