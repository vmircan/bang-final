#include "Panic.h"

Panic::Panic(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Panic, CardTargetType::Targeted, CardRange::RangeDependent) {}

void Panic::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	unsigned choice;
	while (true) {
		std::cout << "Press 1 to draw from another players hand\n";
		std::cout << "\nPress 2 to draw from active cards\n";
		std::cin >> choice;
		if (0 < choice && choice < 3)
			break;
		else
			std::cout << "Invalid choice, choose among the options specified\n";
	}
	if (choice == 1) {
		Player* targetedPlayer = range.getPlayerTargetChoice([](Player* player) {return !player->getNumberOfCards(); });
		if (targetedPlayer) {
			player->drawCard(targetedPlayer->discardRandomCard());
			std::cout << player->getPlayerName() << " has played a " << name << " on " << targetedPlayer->getPlayerName() << "\n";
		}
		else
			state = CardState::Unplayed;
	}
	else {
		Player* targetedPlayer = range.getPlayerTargetChoice([](Player* player) {return !player->getNumberOfActiveCards(); });
		if (targetedPlayer) {
			const std::vector<CardInterface*>& activeCards = targetedPlayer->getActiveCards();
			while (true) {
				for (size_t i = 0; i < activeCards.size(); ++i)
					std::cout << "Press " << i + 1 << " for " << activeCards[i]->getName()<<"\n";
				std::cin >> choice;
				if (0 < choice && choice - 1 < targetedPlayer->getNumberOfActiveCards())
					break;
				else
					std::cout << "Invalid choice, choose among the options specified\n";
			}
			player->drawCard(targetedPlayer->discardActiveCard(choice-1));
			player->setRecalculateRelativeDistances(targetedPlayer->needToRecalculateRelativeDistances());
			targetedPlayer->setRecalculateRelativeDistances(false);
			std::cout << player->getPlayerName() << " has played a " << name << " on " << targetedPlayer->getPlayerName() << "\n";
		}
		else
			state = CardState::Unplayed;
	}
}

void Panic::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
	range.filterRange([](Player* player) {return !player->getNumberOfActiveCards() && !player->getNumberOfCards(); });
}

void Panic::playCard(Player * player, Player * targetedPlayer)
{
}

void Panic::playCard(Player * player, Player * targetedPlayer, CardInterface * targetedCard)
{
	state = CardState::Played;
	if (!targetedCard) {
		std::cout << player->getPlayerName() << " has played a " << name << " on " << targetedPlayer->getPlayerName() << "\n";
		player->drawCard(targetedPlayer->discardRandomCard());
	}
	else {
		std::cout << player->getPlayerName() << " has played a " << name << " on " << targetedPlayer->getPlayerName() << "\n";
		player->drawCard(targetedPlayer->discardActiveCard(targetedCard));
		player->setRecalculateRelativeDistances(targetedPlayer->needToRecalculateRelativeDistances());
		targetedPlayer->setRecalculateRelativeDistances(false);
	}
}

bool Panic::canBePlayed(Player * player) const
{
	return true;
}
