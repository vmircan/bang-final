#pragma once

#include "PlayerIteratorRange.h"
#include "BangPlayer.h"

struct BangPlayerIteratorRange : PlayerIteratorRange {
	BangPlayerIteratorRange() = default;
	BangPlayerIteratorRange(std::vector<BangPlayer*>::iterator begin, std::vector<BangPlayer*>::iterator end) :
		begin(begin), end(end) {}
	BangPlayer* getPlayerTargetChoice(std::function<bool(Player*)> lamda = [](Player*) {return false; }) override;
	void filterRange(std::function<bool(Player*)> lamda = [](Player*) {return false; }) override;
	void for_each(std::function<void(Player*)> lamda) override;
	void clear() override;
	std::vector<BangPlayer*>::iterator begin;
	std::vector<BangPlayer*>::iterator end;
private:
	std::ostream & displayPlayerTargetingOptions(std::ostream & os = std::cout);
};