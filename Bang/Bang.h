#pragma once
#include"Card.h"

class Bang :
	public Card
{
public:
	Bang(const std::string& name, CardColor color, uint8_t number);
	void playCard(Player* player, PlayerIteratorRange & range) override final;
	void playCard(Player* player, Player* targetedPlayer) override;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	bool canBePlayed(Player* player) const override;
};

