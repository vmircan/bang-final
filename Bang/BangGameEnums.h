#pragma once
#include <stdint.h>

namespace BangGameEnums {
	enum class CardType : uint8_t {
		BrownBorderedCard,
		BlueBorderedCard,
	};
	enum class GameState : uint8_t {
		NotFinished,
		BanditVictory,
		RenegadeVictory,
		SheriffVictory,
	};
	enum class BangCharacters
	{
		BartCassidy,
		BlackJack,
		CalamityJones,
		ElGringo,
		JesseJones,
		Jourdonnais,
		KitCarlson,
		LuckyDuke,
		PaulRegret,
		PedroRamirez,
		RoseDoolan,
		SidKetchum,
		SlabTheKiller,
		SuzyLafayette,
		VultureSam,
		WillyTheKid,

		NumberOfCharacters
	};
	enum class Roles : uint8_t {
		Bandit,
		Deputie,
		Renegade,
		Sheriff,
	};
	enum class CardRange : uint8_t {
		RangeDependent,
		RangeIndependent,
	};
	enum class CardTargetType : uint8_t {
		Targeted,
		Untargeted,
	};
	enum class CardState : uint8_t {
		Played,
		Unplayed,
	};
	enum class DamageSource : uint8_t {
		Bang,
		Duel,
		Gatling,
		Indians,
		SlabTheKiller,
		Dynamite,
	};
	enum class CardColor : uint8_t {
		Heart,
		Spades,
		Diamonds,
		Clubs,
	};
	enum class BangCard {
		Bang,
		Barrel,
		Beer,
		CatBalou,
		DrawingCard,
		Duel,
		Dynamite,
		Gatling,
		GeneralStore,
		Indians,
		Jail,
		Missed,
		Mustang,
		Panic,
		Saloon,
		Scope,
		Weapon,
	};
}