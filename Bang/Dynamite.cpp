#include "Dynamite.h"

Dynamite::Dynamite(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Dynamite, CardTargetType::Untargeted) {}

void Dynamite::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (!player->hasDynamite())
		playCard(player, player);
	else {
		std::cout << "Can't play another dynamite if you have one in play\n";
		state = CardState::Unplayed;
	}
}

void Dynamite::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Dynamite::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has put " << name << " in play\n";
	player->setDynamite(true);
}

bool Dynamite::canBePlayed(Player * player) const
{
	return true;
}
