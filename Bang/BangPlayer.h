#pragma once

#include "BangGameEnums.h"
#include "Player.h"
#include "PlayerIteratorRange.h"
#include "Card.h"
#include "StateInterface.h"

#include <algorithm>
#include <iterator>
#include <string>
#include <type_traits>
#include <queue>
#include <vector>

using namespace BangGameEnums;

class BangPlayer : public Player
{
public:
	BangPlayer(const std::string& playerName, Roles role, BangCharacters character, bool computer = false);
	~BangPlayer();
	bool operator==(const BangPlayer & other);
	bool operator!=(const BangPlayer & other);
	const std::string & getPlayerName() override;
	Roles getRole() override;
	bool isComputer() const override;
	inline BangCharacters getCharacter() const override;
	void drawCard(CardInterface* card) override;
	void drawCards(CardInterface* card);
	void addToActiveCards(CardInterface* card) override;
	inline uint8_t getPlayerLifePoints() const override;
	inline uint8_t getPlayerMaxLifePoints() const override;
	inline uint8_t getPlayerRange() const override;
	virtual inline void setPlayerRange(uint8_t range);
	bool decreaseLifePoints(DamageSource source) override;
	inline bool increaseLifePoints() override;
	void discardAllCards();
	CardInterface* discardChosenCard() override;
	CardInterface* discardCard() override;
	CardInterface* discardRandomCard() override;
	CardInterface* discardActiveCard(uint8_t index) override;
	CardInterface* discardActiveCard(CardInterface* cardToDiscard) override;
	inline uint8_t getNumberOfBangsPlayed() const override;
	inline uint8_t getNumberOfBangsPerTurn() const override;
	inline void increaseBangsPlayed() override;
	void resetBangsPlayed();
	inline unsigned getNumberOfCards() const override;
	void resetNumberOfCardsToDraw();
	uint8_t getNumberOfCardsToDraw();
	inline void increaseNumberOfCardsToDraw() override final;
	void putWeaponInPlay(CardInterface* weapon) override;
	inline void setUnlimitedBangs(bool unlimitedBangs) override;
	void displayHand();
	void displayActiveCards();
	inline bool hasDynamite() const override;
	inline void setDynamite(bool dynamite) override;
	unsigned getCardChoice();
	unsigned getTargetChoice();
	inline bool hasMustang() const override;
	inline void setMustang(bool mustang) override;
	inline bool isInJail() const override;
	inline void putInJail() override;
	inline void getOutOfJail() override;
	inline bool hasScope() const override;
	inline void setScope(bool scope) override;
	bool defend(uint8_t defendRequirement);
	inline bool getPlayerChoice();
	bool needToRecalculateRelativeDistances() const override;
	void setRecalculateRelativeDistances(bool recalculate) override;
	bool useBang() override;
	inline uint8_t getNumberOfActiveCards() const override;
	inline void setBarrel(bool barrel) override;
	inline bool hasBarrel() const override;
	void setDamageReduction(uint8_t damageReduction);
	inline void setNumberOfCardsToBeDiscardedFromDeck(uint8_t numberOfCardsToDiscard) override;
	inline uint8_t getNumberOfCardsToBeDiscardedFromDeck() const override;
	void setTheoreticallyUsedCards(uint8_t numberOfCards);
	inline void setPlayedGeneralStore(bool played) override;
	inline bool hasPlayedGeneralStore() const override;
	inline void setPlayedJail(bool played) override;
	inline bool hasPlayedJail() const override;
	const std::vector<CardInterface*>& getActiveCards() const override;
	auto& getPossibleChoices() const;
	void updatePossibleChoices(PlayerIteratorRange& otherPlayers, PlayerIteratorRange& inRangePlayers);

	double getKeepabilityTreshold() const;

	std::vector<CardInterface*>& getCardsToDiscard();

	template<typename ... Args>
	void drawCards(CardInterface* card, Args* ...args) {
		hand.insert(hand.end(), card);
		if (sizeof... (args))
			drawCards(std::forward<Args>(args)...);
	}
	template<typename iterator_type, 
		typename = decltype(*std::declval<iterator_type&>(), ++std::declval<iterator_type&>(), void())>
	void drawCards(iterator_type begin, iterator_type end) {
		std::cout << playerName << " drew cards\n";
		for (; begin != end; begin=std::next(begin))
			hand.emplace_back(*begin);
	}
	const std::vector<CardInterface*> getHand() const override;
	CardInterface* playCard();
	StateInterface* getCurrentState() const;
	StateInterface* getCurrentBestChoice();
	void clearPossibleChoices();
	StateInterface* getBestChoice();
	CardInterface* getBestPlay();
	CardInterface* getCurrentBestPlay();
	
	CardInterface* decideOnDiscard() override;
	bool decideOnDefend() const;
	int decideOnDefend(const std::vector<std::pair<uint8_t, uint8_t>>& possibleDefendingOptions) const;
	bool decideOnBangUse() override;
	bool decideOnDiscardingCardsForLife() const;
	Player* decideOnDrawingFromAnotherPlayer() const override;


	void evaluateCard(CardInterface* card);
	void evaluateCardReverseCost(CardInterface* card);
	void evaluateCards();
	void addToTotalCost(double cost) override;
	double getAverageHandValue() const override;
	void calculateAverageHandValue() override;

	template<typename ... Args>
	void evaluateCards(CardInterface* card, Args* ...args) {
		evaluateCard(card);
		if (sizeof... (args))
			evaluateCards(std::forward<Args>(args)...);
	}

	template<typename ... Args>
	void evaluateCardsReverseCost(CardInterface* card, Args* ...args) {
		evaluateCardReverseCost(card);
		if (sizeof... (args))
			evaluateCardsReverseCost((args)...);
	}

	//template<typename iterator_type, typename std::enable_if<not std::is_integral<iterator_type>::value
	//	or std::is_base_of <std::random_access_iterator_tag
	//	, typename std::iterator_traits<iterator_type>::iterator_category>::value>::type>
	//	void drawCards(iterator_type begin, iterator_type end) {
	//	for(begin;begin!=end; begin=std::next(begin))
	//		hand.insert(hand.end(), std::move(*begin));
	//}


	//template<typename iterator_type, typename std::enable_if<std::is_base_of<std::random_access_iterator_tag, typename std::iterator_traits<iterator_type>::iterator_category>::value ||
		//std::is_same<std::bidirectional_iterator_tag, typename std::iterator_traits<iterator_type>::iterator_category>::value>::type>

	//template<typename iterator_type, typename = std::enable_if<is_iterator<, std::_Iterator_base>::value>>

	//template<typename iterator_type, typename std::enable_if<std::is_base_of<std::iterator<std::random_access_iterator_tag, iterator_type>

private:
	void evaluateCards(CardInterface* card);
	void evaluateCardsReverseCost(CardInterface* card);

private:
	const std::string playerName;
	bool computer;
	std::priority_queue < StateInterface*, std::vector<StateInterface*>, std::function<bool(StateInterface*, StateInterface*)>> possibleChoices;
	StateInterface* currentState;
	StateInterface* lastInsertedState;
	const Roles role;
	const BangCharacters character;
	std::vector<CardInterface*> hand;
	std::vector<CardInterface*> activeCards;
	double totalHandValue =0;
	double averageHandValue = 0;
	uint8_t numberOfEvaluatedCards = 0;
	uint8_t maxLifePoints, lifePoints, range;
	uint8_t bangsPerTurn, bangsPlayedThisTurn;
	uint8_t numberOfCardsToDraw = 0;
	std::vector<CardInterface*> cardsToDiscard;
	bool dynamite = false;
	bool mustang = false;
	bool jail = false;
	bool scope = false;
	bool barrel = false;
	bool recalculateRelativeDistances = false;
	bool playedGeneralStore = false;
	bool playedJail = false;
	uint8_t damageReduction = 0;
	uint8_t numberOfCardsToBeDiscardedFromDeck = 0;
	uint8_t numberOfTheoreticallyUsedCards = 0;
	const double playabilityTreshold = 30;
	const double keepabilityTreshold = 12;
};

