#pragma once

#include "BangGameEnums.h"
#include "CardInterface.h"
#include "PlayerIteratorRange.h"

#include <iostream>
#include <iterator>
#include <string>
#include <type_traits>
#include <vector>

using namespace BangGameEnums;

class Player
{
public:
	virtual const std::string& getPlayerName() = 0;
	virtual inline BangCharacters getCharacter() const = 0;
	virtual inline Roles getRole() = 0;
	virtual bool isComputer() const = 0;
	virtual inline uint8_t getPlayerRange() const = 0;
	virtual inline void setPlayerRange(uint8_t range) = 0;
	virtual inline uint8_t getPlayerLifePoints() const = 0;
	virtual inline uint8_t getPlayerMaxLifePoints() const = 0;
	virtual bool decreaseLifePoints(DamageSource source) = 0;
	virtual inline bool increaseLifePoints() = 0;
	virtual CardInterface* discardChosenCard() = 0;
	virtual CardInterface* discardCard() = 0;
	virtual CardInterface* discardRandomCard() = 0;
	virtual CardInterface* discardActiveCard(uint8_t index) = 0;
	virtual CardInterface* discardActiveCard(CardInterface* cardToDiscard) = 0;
	virtual void drawCard(CardInterface* card) = 0;
	virtual void addToActiveCards(CardInterface* card) = 0;
	virtual void putWeaponInPlay(CardInterface* weapon) = 0;
	virtual inline void setUnlimitedBangs(bool unlimitedBangs) = 0;
	virtual inline uint8_t getNumberOfBangsPlayed() const = 0;
	virtual inline uint8_t getNumberOfBangsPerTurn() const = 0;
	virtual inline void increaseBangsPlayed() = 0;
	virtual inline unsigned getNumberOfCards() const = 0;
	virtual inline uint8_t getNumberOfActiveCards() const = 0;
	virtual inline void increaseNumberOfCardsToDraw() = 0;
	virtual inline bool hasMustang() const = 0;
	virtual inline void setMustang(bool mustang) = 0;
	virtual inline bool isInJail() const = 0;
	virtual inline void putInJail() = 0;
	virtual inline void getOutOfJail() = 0;
	virtual inline bool hasScope() const = 0;
	virtual inline void setScope(bool scope) = 0;
	virtual inline bool hasDynamite() const = 0;
	virtual inline void setDynamite(bool dynamite) = 0;
	virtual bool needToRecalculateRelativeDistances() const = 0;
	virtual void setRecalculateRelativeDistances(bool recalculate) = 0;
	virtual bool useBang() = 0;
	virtual void setBarrel(bool barrel) = 0;
	virtual bool hasBarrel() const = 0;
	virtual inline void setNumberOfCardsToBeDiscardedFromDeck(uint8_t numberOfCardsToDiscard) = 0;
	virtual inline uint8_t getNumberOfCardsToBeDiscardedFromDeck() const = 0;
	virtual inline bool hasPlayedGeneralStore() const = 0;
	virtual inline void setPlayedGeneralStore(bool played) = 0;
	virtual inline bool hasPlayedJail() const = 0;
	virtual inline void setPlayedJail(bool played) = 0;
	virtual const std::vector<CardInterface*>& getActiveCards() const = 0;
	virtual Player* decideOnDrawingFromAnotherPlayer() const = 0;
	virtual CardInterface* decideOnDiscard() = 0;
	virtual bool decideOnBangUse() = 0;
	virtual void addToTotalCost(double cost) = 0;
	virtual double getAverageHandValue() const = 0;
	virtual void calculateAverageHandValue() = 0;
	virtual const std::vector<CardInterface*> getHand() const = 0;
};


