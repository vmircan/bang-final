#include "CatBalou.h"

CatBalou::CatBalou(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::CatBalou, CardTargetType::Targeted) {}

void CatBalou::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	Player* targetedPlayer = range.getPlayerTargetChoice([](Player* player) {return !player->getNumberOfCards(); });
	if (targetedPlayer)
		playCard(player, targetedPlayer);
	else
		state = CardState::Unplayed;
}

void CatBalou::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
	range.filterRange([](Player* player) {return !player->getNumberOfCards(); });
}

void CatBalou::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << " on " << targetedPlayer->getPlayerName() << "\n";
	if (targetedPlayer->isComputer())
		targetedPlayer->decideOnDiscard();
	else
		targetedPlayer->discardChosenCard();
}

bool CatBalou::canBePlayed(Player * player) const
{
	return false;
}
