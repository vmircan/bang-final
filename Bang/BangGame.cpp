#include "BangGame.h"
#include "Bang.h"
#include "Barrel.h"
#include "Beer.h"
#include "CatBalou.h"
#include "DrawingCard.h"
#include "Duel.h"
#include "Dynamite.h"
#include "Gatling.h"
#include "GeneralStore.h"
#include "Indians.h"
#include "Jail.h"
#include "Missed.h"
#include "Mustang.h"
#include "Panic.h"
#include "Saloon.h"
#include "Scope.h"
#include "temp.h"
#include "Weapon.h"
#include "State.h"

#include <algorithm>
#include <chrono>
#include <deque>
#include <fstream>
#include <iostream>
#include <random>
#include <set>
#include <unordered_set>


BangGame::BangGame(unsigned numberOfPlayers, unsigned numberOfComputers) : 
	numberOfPlayers(numberOfPlayers), numberOfComputers(numberOfComputers)
{
}

BangGame::~BangGame()
{
}

uint8_t BangGame::getNumberOfPlayers()
{
	return numberOfPlayers;
}

BangPlayer * BangGame::getPlayer(uint8_t index)
{
	if (index < players.size())
		return players[index];
	else
		return nullptr;
}

void BangGame::distributeRolesAndCharacters()
{
	std::deque<Roles> availableRoles;
	switch (numberOfPlayers)
	{
	case 4:
		availableRoles = std::deque<Roles>{
			Roles::Sheriff, Roles::Bandit,
			Roles::Bandit, Roles::Renegade
		};
		sheriff = true;
		banditCount = 2;
		renegade = true;
		break;
	case 5:
		availableRoles = std::deque<Roles>{
			Roles::Sheriff, Roles::Deputie, Roles::Bandit,
			Roles::Bandit, Roles::Renegade
		};
		sheriff = true;
		banditCount = 2;
		renegade = true;
		deputieCount = 1;
		break;
	case 6:
		availableRoles = std::deque<Roles>{
			Roles::Sheriff, Roles::Deputie, Roles::Bandit,
			Roles::Bandit, Roles::Bandit, Roles::Renegade
		};
		sheriff = true;
		banditCount = 3;
		renegade = true;
		deputieCount = 1;
		break;
	case 7:
		availableRoles = std::deque<Roles>{
			Roles::Sheriff, Roles::Deputie, Roles::Deputie, Roles::Bandit,
			Roles::Bandit, Roles::Bandit, Roles::Renegade
		};
		sheriff = true;
		banditCount = 3;
		renegade = true;
		deputieCount = 2;
		break;
	default:
		break;
	}
	std::deque<BangCharacters> availableCharacters(static_cast<int>(BangCharacters::NumberOfCharacters));
	uint8_t currentCharacterIndex = 0;
	std::generate(availableCharacters.begin(), availableCharacters.end(),
		[&currentCharacterIndex]() {return static_cast<BangCharacters>(currentCharacterIndex++); });
	std::default_random_engine engine;
	//engine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	for (uint8_t i = 0; i < numberOfPlayers; ++i)
	{
		bool computer = false;
		std::uniform_int_distribution<size_t> characterDistribution(0, availableCharacters.size() - 1);
		std::uniform_int_distribution<size_t> roleDistribution(0, availableRoles.size() - 1);
		std::string playerName;
		if (!(i < numberOfPlayers - numberOfComputers)) {
			playerName = std::string("Computer #") + std::to_string(i - (numberOfPlayers - numberOfComputers) + 1);
			computer = true;
		}
		else {
			std::cout << "Player #" << i + 1 << " name:\n";
			std::cin >> playerName;
		}
		uint8_t chosenCharacterIndex(static_cast<uint8_t>(characterDistribution(engine))),
			chosenRoleIndex(static_cast<uint8_t>(roleDistribution(engine)));
		if (availableCharacters[chosenCharacterIndex] == BangCharacters::PaulRegret ||
			availableCharacters[chosenCharacterIndex] == BangCharacters::RoseDoolan)
			distanceAugmentations = true;
		players.emplace_back(new BangPlayer(playerName, availableRoles[chosenRoleIndex],
			availableCharacters[chosenCharacterIndex], computer));
		availableCharacters.erase(availableCharacters.cbegin() + chosenCharacterIndex);
		availableRoles.erase(availableRoles.cbegin() + chosenRoleIndex);
	}
}

std::ostream& BangGame::displayRolesAndCharacters(std::ostream& os=std::cout)
{
	os << "\n";
	for (BangPlayer* player : players) {
		os << player->getPlayerName() << " is ";
		switch (player->getCharacter())
		{
		case BangGameEnums::BangCharacters::BartCassidy:
			os << "BartCassidy";
			break;
		case BangGameEnums::BangCharacters::BlackJack:
			os << "BlackJack";
			break;
		case BangGameEnums::BangCharacters::CalamityJones:
			os << "CalamityJones";
			break;
		case BangGameEnums::BangCharacters::ElGringo:
			os << "ElGringo";
			break;
		case BangGameEnums::BangCharacters::JesseJones:
			os << "JesseJones";
			break;
		case BangGameEnums::BangCharacters::Jourdonnais:
			os << "Jourdonnais";
			break;
		case BangGameEnums::BangCharacters::KitCarlson:
			os << "KitCarlson";
			break;
		case BangGameEnums::BangCharacters::LuckyDuke:
			os << "LuckyDuke";
			break;
		case BangGameEnums::BangCharacters::PaulRegret:
			os << "PaulRegret";
			break;
		case BangGameEnums::BangCharacters::PedroRamirez:
			os << "PedroRamirez";
			break;
		case BangGameEnums::BangCharacters::RoseDoolan:
			os << "RoseDoolan";
			break;
		case BangGameEnums::BangCharacters::SidKetchum:
			os << "SidKetchum";
			break;
		case BangGameEnums::BangCharacters::SlabTheKiller:
			os << "SlabTheKiller";
			break;
		case BangGameEnums::BangCharacters::SuzyLafayette:
			os << "SuzyLafayette";
			break;
		case BangGameEnums::BangCharacters::VultureSam:
			os << "VultureSam";
			break;
		case BangGameEnums::BangCharacters::WillyTheKid:
			os << "WillyTheKid";
			break;
		}
		os << " and has the role of ";
		switch (player->getRole())
		{
		case BangGameEnums::Roles::Bandit:
			os << "Bandit";
			break;
		case BangGameEnums::Roles::Deputie:
			os << "Deputie";
			break;
		case BangGameEnums::Roles::Renegade:
			os << "Renegade";
			break;
		case BangGameEnums::Roles::Sheriff:
			os << "Sheriff";
			break;
		}
		os << "\n\n";
	}
	return os;
}

void BangGame::setRelativeDistances()
{
	if (!table.size()) {
		for (BangPlayer* player : players)
			table.add(player);
		relativeDistances.reserve(numberOfPlayers);
	}
	else {
		relativeDistances.clear();
	}
	if (distanceAugmentations)
		return setAugmentedRelativeDistances();
	unsigned distance = 0;
	unsigned counter = 0;
	std::vector<std::vector<std::pair<uint8_t, uint8_t>>> relativeDistancesVector(1);
	for (CircularList<BangPlayer>::iterator it = table.begin(); it != table.end(); ++it, ++distance, ++counter)
		relativeDistancesVector[0].emplace_back(distance, counter);
	distance = 1;
	size_t index = relativeDistancesVector[0].size() - 1;
	for (CircularList<BangPlayer>::reverse_iterator it = table.rbegin(); it != table.rend(); ++it, ++distance, --index)
		if (distance < relativeDistancesVector[0][index].first)
			relativeDistancesVector[0][index].first = distance;
	for (size_t i = 1; i < numberOfPlayers; ++i)
	{
		relativeDistancesVector.emplace_back(numberOfPlayers);
		std::rotate_copy(relativeDistancesVector[i - 1].begin(), relativeDistancesVector[i - 1].begin() + relativeDistancesVector[i - 1].size() - 1, relativeDistancesVector[i - 1].end(), relativeDistancesVector.back().begin());
		std::transform(relativeDistancesVector.back().begin(), relativeDistancesVector.back().end(), relativeDistancesVector.back().begin(),
			[this](std::pair<uint8_t, uint8_t>& distancePair) {
				return std::make_pair(distancePair.first, (distancePair.second + 1) % numberOfPlayers);
		});
	}
	for (const std::vector<std::pair<uint8_t, uint8_t>>& playerRelativeDistancesVector : relativeDistancesVector) {
		relativeDistances.emplace_back();
		for (const std::pair<uint8_t, uint8_t>& distancePair : playerRelativeDistancesVector)
			relativeDistances.back().emplace(distancePair);
	}
}
template<typename T>
T min(T first, T second) {
	if (second < first)
		return second;
	return first;
}
void BangGame::setAugmentedRelativeDistances()
{
	for (int i = 0; i < numberOfPlayers; ++i)
	{
		relativeDistances.emplace_back();
		for (int j = 0; j < numberOfPlayers; ++j)
		{
			unsigned distance = min(min(abs(i - j), numberOfPlayers - j + i), numberOfPlayers-i+j);
			if (j != i) {
				if (players[j]->getCharacter() == BangCharacters::PaulRegret)
					++distance;
				if (players[j]->hasMustang())
					++distance;
				if (players[i]->getCharacter() == BangCharacters::RoseDoolan)
					if (distance > 1)
						--distance;
				if (players[i]->hasScope())
					if (distance > 1)
						--distance;
			}
			relativeDistances.back().emplace(distance, j);
		}
	}
}

void BangGame::distributeCards()
{
	for (BangPlayer* player : players)
			giveCardsFromDeckToPlayer(player, player->getPlayerLifePoints());
}

void BangGame::removePlayer(BangPlayer* player)
{
	switch (player->getRole())
	{
	case Roles::Bandit:
		--banditCount;
		break;
	case Roles::Renegade:
		renegade=false;
		break;
	case Roles::Sheriff:
		sheriff=false;
		break;
	case Roles::Deputie:
		--deputieCount;
		break;
	default:
		break;
	}
	uint8_t playerIndex = playerIndexMap[player];
	std::vector<BangPlayer*>::const_iterator playerToRemove = players.cbegin() + playerIndex;
	table.remove(playerIndex);
	players.erase(playerToRemove);
	--numberOfPlayers;
	updatePlayerIndexMap();
	setRelativeDistances();
}
void BangGame::updatePlayerIndexMap()
{
	playerIndexMap.clear();
	for (size_t i = 0; i < players.size(); ++i)
		playerIndexMap[players[i]] = static_cast<uint8_t>(i);
}
void BangGame::initializeDeck()
{
	std::default_random_engine engine, shuffler;
	//engine.seed(std::chrono::system_clock::now().time_since_epoch().count);
	//shuffler.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<unsigned> cardColorDistribution(0, 3);
	std::uniform_int_distribution<unsigned> cardNumberDistribution(1, 14);

	deck.reserve(numberOfCards);
	for (size_t i = 0; i < 25; ++i)
		deck.push_back(new Bang("Bang", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 12; ++i)
		deck.push_back(new Missed("Missed", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 6; ++i)
		deck.push_back(new Beer("Beer", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 4; ++i)
		deck.push_back(new Panic("Panic", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 4; ++i)
		deck.push_back(new CatBalou("Cat Balou", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new DrawingCard("Stagecoach", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine)), 2));
	deck.push_back(new DrawingCard("Wells Fargo", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine)), 3));
	deck.push_back(new Gatling("Gatling", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 3; ++i)
		deck.push_back(new Duel("Duel", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new Indians("Indians", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new GeneralStore("General Store", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	deck.push_back(new Saloon("Saloon", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 3; ++i)
		deck.push_back(new Jail("Jail", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	deck.push_back(new Dynamite("Dynamite", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new Barrel("Barrel", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	deck.push_back(new Scope("Scope", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new Mustang("Mustang", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine))));
	for (size_t i = 0; i < 3; ++i)
		deck.push_back(new Weapon("Schofield", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine)), 2));
	for (size_t i = 0; i < 2; ++i)
		deck.push_back(new Weapon("Volcanic", static_cast<CardColor>(cardColorDistribution(engine)),
			static_cast<uint8_t>(cardNumberDistribution(engine)), 1, true));
	deck.push_back(new Weapon("Rev. Carabine", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine)), 4));
	deck.push_back(new Weapon("Remington", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine)), 3));
	deck.push_back(new Weapon("Winchester", static_cast<CardColor>(cardColorDistribution(engine)),
		static_cast<uint8_t>(cardNumberDistribution(engine)), 5));
	std::shuffle(deck.begin(), deck.end(), shuffler);

}
std::ostream& BangGame::printDeck(std::ostream& os) const {
	for (Card* c : deck)
		os << c->getName()<< "\n";
	return os;
}
void BangGame::refreshDeck()
{
	auto shuffler = std::default_random_engine{};
	std::shuffle(discardedCards.begin(), discardedCards.end(), shuffler);
	deck.insert(deck.end(), std::make_move_iterator(discardedCards.begin()), std::make_move_iterator(discardedCards.end()));
	discardedCards.clear();
}
void BangGame::checkForDistanceAugmentations() {
	distanceAugmentations = false;
	for (BangPlayer* player : players)
		if (player->hasMustang() || player->hasScope() || player->getCharacter() == BangCharacters::PaulRegret ||
			player->getCharacter() == BangCharacters::RoseDoolan) {
			distanceAugmentations = true;
			return;
		}
}
void BangGame::handleKill(BangPlayer * activePlayer, BangPlayer * targetedPlayer)
{
	BangPlayer* deadPlayer = targetedPlayer? targetedPlayer:activePlayer;
	if (targetedPlayer) {
		displayPlayerDeath(activePlayer, targetedPlayer);
		if (targetedPlayer->getRole() == Roles::Bandit)
			giveCardsFromDeckToPlayer(activePlayer, 3);
		else if (activePlayer->getRole() == Roles::Sheriff && targetedPlayer->getRole() == Roles::Deputie) {
			activePlayer->discardAllCards();
			for (CardInterface* discardedCard : activePlayer->getCardsToDiscard())
				discardedCards.push_back(dynamic_cast<Card*>(discardedCard));
		}
	}
	else
		displayPlayerDeath(activePlayer);
	for (BangPlayer* player : players) {
		if (player->getCharacter() == BangCharacters::VultureSam && *player!=*deadPlayer) {
			uint8_t cardNumber = deadPlayer->getNumberOfCards();
			for (size_t i = 0; i < cardNumber; ++i)
				player->drawCards(deadPlayer->discardCard());
		}
	}
	deadPlayer->discardAllCards();
	std::vector<CardInterface*> cardsToDiscard=deadPlayer->getCardsToDiscard();
	for (CardInterface* card : cardsToDiscard) {
		discardedCards.insert(discardedCards.end(), dynamic_cast<Card*>(card));
	}
	cardsToDiscard.clear();
	removePlayer(deadPlayer);
	checkForDistanceAugmentations();
	gameState = checkGameState();

	if(gameState!= GameState::NotFinished)
		handleVictory(gameState);
}
void BangGame::displayPlayerDeath(BangPlayer * activePlayer, BangPlayer * targetedPlayer) const
{
	std::cout << targetedPlayer->getPlayerName() << " was killed by " << activePlayer->getPlayerName() << "\n";
}
void BangGame::displayPlayerDeath(BangPlayer * killedPlayer) const
{
	std::cout << killedPlayer->getPlayerName() << " was killed by dynamite\n";
}
void BangGame::handleVictory(GameState victory)
{
	switch (victory)
	{
	case GameState::BanditVictory:
		std::cout << "\nBandits won\n";
		break;
	case GameState::RenegadeVictory:
		std::cout << "\nRenegade won\n";
		break;
	case GameState::SheriffVictory:
		std::cout << "\nSheriff/Deputie victory\n";
		break;
	default:
		break;
	}
}
void BangGame::giveCardsFromDeckToPlayer(BangPlayer* player, unsigned numberOfCards)
{
	if (deck.size()<numberOfCards) {
		size_t cardsDrew = deck.size();
		player->drawCards(std::make_move_iterator(deck.begin()), std::make_move_iterator(deck.end()));
		deck.clear();
		refreshDeck();
		player->drawCards(std::make_move_iterator(deck.rbegin()), std::make_move_iterator(deck.rbegin() + numberOfCards-cardsDrew));
		for (size_t i = numberOfCards-cardsDrew; i > 0; --i)
			deck.pop_back();
	}
	else {
		player->drawCards(std::make_move_iterator(deck.rbegin()), std::make_move_iterator(deck.rbegin() + numberOfCards));
		for (size_t i = 0; i < numberOfCards; ++i)
			deck.pop_back();
	}
}
void BangGame::giveCardToPlayer(BangPlayer * player, Card * card)
{
	if (deck.size())
		player->drawCard(card);
	else {
		refreshDeck();
		player->drawCard(card);
	}
}
GameState BangGame::checkGameState()
{
	if (!sheriff) {
		if (players.size() == 1 && renegade)
			return GameState::RenegadeVictory;
		else
			return GameState::BanditVictory;
	}
	else if(!banditCount && ! renegade)
		return GameState::SheriffVictory;
	else
		return GameState::NotFinished;
}
BangPlayerIteratorRange& BangGame::getPlayersInRange(BangPlayer * player, uint8_t range)
{
	return getPlayersInRange(playerIndexMap[player], range);
}
BangPlayerIteratorRange& BangGame::getPlayersInRange(uint8_t playerIndex, uint8_t range)
{
	if (!(playerIndex < numberOfPlayers)) {
		playersInRange = BangPlayerIteratorRange();
		return playersInRange;
	}
	std::multimap<uint8_t, uint8_t>::const_iterator begin = relativeDistances[playerIndex].lower_bound(1);
	std::multimap<uint8_t, uint8_t>::const_iterator end = relativeDistances[playerIndex].upper_bound(range);
	std::unordered_set<uint8_t> inRangePlayerIndicesSet;
	std::transform(begin, end, std::inserter(inRangePlayerIndicesSet, inRangePlayerIndicesSet.begin()),
		[](const std::pair<uint8_t, uint8_t>& distancePair) {
			return distancePair.second;
		}
	);
	std::vector<BangPlayer*>::iterator inRangePlayersEnd = std::remove_if(playersCopy.begin(), playersCopy.end(),
		[this, playerIndex, &inRangePlayerIndicesSet] (BangPlayer* player) {
			return inRangePlayerIndicesSet.find(playerIndexMap[player]) == inRangePlayerIndicesSet.end() || *playersCopy[playerIndex]==*player;
		}
	);
	playersInRange = BangPlayerIteratorRange(playersCopy.begin(), inRangePlayersEnd);
	return playersInRange;
}
BangPlayerIteratorRange& BangGame::getPlayersInRange()
{
	return getPlayersInRange(activePlayerIndex, activePlayer->getPlayerRange());
}
BangPlayerIteratorRange& BangGame::getOtherPlayers(BangPlayer* player)
{

	std::vector<BangPlayer*>::iterator end = std::remove_if(playersCopy2.begin(), playersCopy2.end(), [player](BangPlayer* currentPlayer) {
		return *currentPlayer == *player;
	});
	otherPlayers = BangPlayerIteratorRange(playersCopy2.begin(), end);
	return otherPlayers;
}
BangPlayerIteratorRange& BangGame::getOtherPlayers()
{
	return getOtherPlayers(activePlayer);
}
std::vector<Player*> BangGame::getOtherPlayersVector() const
{
	std::vector<Player*> temp(players.size());
	std::copy_if(players.begin(), players.end(), temp.begin(), [this](Player* player) {
		return player != activePlayer; });
	return temp;
}
inline BangPlayer& BangGame::getActivePlayer()
{
	return *activePlayer;
}
inline uint8_t BangGame::getActivePlayerIndex()
{
	return activePlayerIndex;
}
void BangGame::endTurn(BangPlayer * player)
{
	passDynamiteToPlayer = false;
	if (player->hasDynamite())
		passDynamiteToPlayer = true;
	player->resetBangsPlayed();
	int numberOfCardsToDiscard = static_cast<int>(player->getNumberOfCards()) - static_cast<int>(player->getPlayerLifePoints());
	if (numberOfCardsToDiscard > 0) {
		if (!player->isComputer()) {
			std::cout << player->getPlayerName() << " needs to discard " << numberOfCardsToDiscard << " cards\n";
			for (int i = 0; i < numberOfCardsToDiscard; ++i)
				discardedCards.push_back(dynamic_cast<Card*>(player->discardChosenCard()));
		}
		else {
			for (auto card : player->getHand())
				player->evaluateCardReverseCost(card);
			for (size_t i = 0; i < numberOfCardsToDiscard; ++i)
				discardedCards.push_back(dynamic_cast<Card*>(player->getCurrentBestPlay()));
			player->clearPossibleChoices();
		}
	}
	std::cout << player->getPlayerName() << " has finished his turn\n\n";
}
void BangGame::drawPhase(BangPlayer * player)
{
	unsigned cardsToDraw = cardsPerTurn;
	std::vector<Card*> topCardsOfDeck;
	Card* topOfDeck;
	unsigned counter = 0;
	switch (player->getCharacter())
	{
	case BangCharacters::BlackJack:
		giveCardsFromDeckToPlayer(activePlayer, 1);
		--cardsToDraw;
		topOfDeck = deck.back();
		std::cout << player->getPlayerName() << " has drawn a " << topOfDeck->getName() << "\n";
		if (topOfDeck->getColor() == CardColor::Heart || topOfDeck->getColor() == CardColor::Diamonds) {
			/*std::cout << "Card is " << topOfDeck->getColor() << "; " << */
			std::cout<<player->getPlayerName() << " draws an additional card\n";
			++cardsToDraw;
		}
		giveCardsFromDeckToPlayer(activePlayer, cardsToDraw);
		break;
	case BangCharacters::KitCarlson:
		if (2 < deck.size()) {
			topCardsOfDeck.insert(topCardsOfDeck.end(), deck.rbegin(), deck.rbegin() + 3);
		}
		else {
			topCardsOfDeck.insert(topCardsOfDeck.end(), deck.begin(), deck.end());
			deck.clear();
			refreshDeck();
			topCardsOfDeck.insert(topCardsOfDeck.end(), deck.rbegin(), (deck.rbegin() + 3 - topCardsOfDeck.size()));
		}
		if (!player->isComputer()) {
			char charChoice;
			unsigned choice;
			do {
				std::cout << "Choose a card to discard from the following cards:\n";
				for (size_t i = 0; i < topCardsOfDeck.size();)
					std::cout << "Press " << ++i << " for " << topCardsOfDeck[i]->getName() << "\n";
				std::cin >> charChoice;
				choice = (int)(charChoice - '0') - 1;
				if (choice < topCardsOfDeck.size()) {
					deck.push_back(topCardsOfDeck[choice]);
					topCardsOfDeck.erase(topCardsOfDeck.begin() + choice);
				}
				else {
					std::cout << "Invalid choice\n";
				}
			} while (!(choice < topCardsOfDeck.size()));
			for (Card* card : topCardsOfDeck)
				giveCardToPlayer(player, card);
		}
		else {
			player->evaluateCardsReverseCost(topCardsOfDeck[0], topCardsOfDeck[1], topCardsOfDeck[2]);
			CardInterface* cardToDiscard = dynamic_cast<Card*>(player->getBestPlay());
			topCardsOfDeck.erase(std::find(topCardsOfDeck.begin(), topCardsOfDeck.end(), cardToDiscard));
			for (auto card : topCardsOfDeck)
				giveCardToPlayer(player, card);
		}
		break;
	case BangCharacters::PedroRamirez:
		if((!player->isComputer() && promptPlayerForAlternativeDrawingMechanic(player)))
		{
			if (discardedCards.size()) {
				giveCardToPlayer(player, discardedCards.back());
				--cardsToDraw;
			}
			else
				std::cout << "Discarded pile is empty\n";
		}
		else if (player->isComputer() && discardedCards.size()) {
			player->clearPossibleChoices();
			player->evaluateCard(discardedCards.back());
			if (!(player->getBestChoice()->getGCost() < player->getKeepabilityTreshold())) {
				giveCardToPlayer(player, discardedCards.back());
				--cardsToDraw;
			}
		}
		giveCardsFromDeckToPlayer(activePlayer, cardsToDraw);
		break;
	case BangCharacters::JesseJones:
		if (!player->isComputer() && promptPlayerForAlternativeDrawingMechanic(player))
		{
			BangPlayerIteratorRange range = getOtherPlayers();
			BangPlayer* targetedPlayer = range.getPlayerTargetChoice([]
			(Player* player) {return !player->getNumberOfCards(); });
			if (targetedPlayer) {
				player->drawCard(targetedPlayer->discardRandomCard());
				std::cout << player->getPlayerName() << " drew from " << targetedPlayer->getPlayerName() << "\n";
				--cardsToDraw;
			}
			else
				std::cout << "No player has any cards to draw from\n";
		}
		else if(player->isComputer()) {
			Player* targetedPlayer = player->decideOnDrawingFromAnotherPlayer();
			if (targetedPlayer) {
				player->drawCard(targetedPlayer->discardRandomCard());
				std::cout << player->getPlayerName() << " drew from " << targetedPlayer->getPlayerName() << "\n";
				--cardsToDraw;
			}
		}
		giveCardsFromDeckToPlayer(activePlayer, cardsToDraw);
		break;
	default:
		giveCardsFromDeckToPlayer(activePlayer, cardsPerTurn);
	}
}
void BangGame::luckDrawPhase(BangPlayer * activePlayer)
{
	bool dynamiteExploded = false;
	if (testForDynamiteExplosion(activePlayer)) {
		dynamiteExploded = true;
		if (activePlayer->getCharacter() == BangCharacters::LuckyDuke)
			if (!testForDynamiteExplosion(activePlayer))
				dynamiteExploded = false;
	}
	if (dynamiteExploded) {
		std::cout << "\nDynamite exploded\n";
		activePlayer->setDynamite(false);
		for (size_t i = 0; i < dynamiteDamage; ++i)
			activePlayer->decreaseLifePoints(DamageSource::Dynamite);
		std::cout << activePlayer->getPlayerName() << " has lost " << dynamiteDamage << " life points to dynamite\n";
	}
	else if(activePlayer->hasDynamite())
		std::cout << "\nDynamite didn't explode this turn\n";
	if (activePlayer->isInJail()) {
		bool getOutOfJail = false;
		if (testForGettingOutOfJail(activePlayer))
			getOutOfJail = true;
		else if (activePlayer->getCharacter() == BangCharacters::LuckyDuke && testForGettingOutOfJail(activePlayer))
			getOutOfJail = true;
		if (getOutOfJail) {
			std::cout << activePlayer->getPlayerName() << " has gotten out of jail\n";
			activePlayer->getOutOfJail();
		}
	}
}
bool BangGame::promptPlayerForAlternativeDrawingMechanic(BangPlayer * player)
{
	if (player->getCharacter() == BangCharacters::PedroRamirez)
		std::cout << "Draw from discarded cards? (Y/N)\n";
	else if(player->getCharacter()==BangCharacters::JesseJones)
		std::cout << "Draw from another player? (Y/N)\n";
	char choice;
	std::cin>>choice;
	if (tolower(choice) == 'y')
		return true;
	return false;

}
bool BangGame::promptPlayerToDiscardCardsForLife(BangPlayer * player)
{
	if (1 < player->getNumberOfCards()) {
		if (player->isComputer())
			return player->decideOnDiscardingCardsForLife();
		std::cout << "Discard 2 cards for 1 life point? (Y/N)\n";
		char choice;
		std::cin>>choice;
		if (tolower(choice) == 'y')
			return true;
	}
	return false;
}
bool BangGame::promptPlayerForEndOfTurn(BangPlayer * player)
{
	std::cout <<"\n"<< player->getPlayerName()<<", end turn? (Y/N)\n";
	char choice;
	std::cin>>choice;
	if (tolower(choice) == 'y')
		return true;
	return false;
}
bool BangGame::checkForPlayerDeath(BangPlayer * activePlayer)
{
	for (BangPlayer* player : players) {
		if (!player->getPlayerLifePoints()) {
			handleKill(activePlayer, player);
			return true;
		}
	}
	return false;
}
bool BangGame::checkForActivePlayerDeath(BangPlayer * activePlayer)
{
	if (!activePlayer->getPlayerLifePoints()) {
		handleKill(activePlayer);
		return true;
	}
	return false;
}
bool BangGame::testForDynamiteExplosion(BangPlayer * activePlayer)
{
	if (activePlayer->hasDynamite()) {
		if (!deck.size())
			refreshDeck();
		Card* topOfDeck = deck.back();
		deck.pop_back();
		discardedCards.push_back(topOfDeck);
		if (2 < topOfDeck->getNumber() && topOfDeck->getNumber() < 9 && 
			topOfDeck->getColor() == CardColor::Spades)
			return true;
		else
			return false;
	}
	return false;
}
bool BangGame::testForGettingOutOfJail(BangPlayer * activePlayer)
{
	if (activePlayer->isInJail()) {
		if (!deck.size())
			refreshDeck();
		Card* topOfDeck = deck.back();
		deck.pop_back();
		discardedCards.push_back(topOfDeck);
		if (topOfDeck->getColor()==CardColor::Heart)
			return true;
		else
			return false;
	}
	return false;
}
bool BangGame::testForGettingOutOfJail()
{
	return testForGettingOutOfJail(activePlayer);
}
void BangGame::checkForBarrels()
{
	if (!deck.size())
		refreshDeck();
	uint8_t damageReduction = deck.back()->getColor() == CardColor::Heart ? 1 : 0;
	for (BangPlayer* player : players) {
		if (player->hasBarrel()) {
			if (player->getCharacter() == BangCharacters::LuckyDuke) {
				if (!damageReduction) {
					if(1<deck.size())
						player->setDamageReduction(deck[deck.size() - 2]->getColor() == CardColor::Heart ? 1 : 0);
					else {
						std::default_random_engine e;
						std::uniform_int_distribution<unsigned> distribution(0, discardedCards.size() - 1);
						player->setDamageReduction(discardedCards[distribution(e)]->getColor() == CardColor::Heart ? 1 : 0);
					}
					player->setTheoreticallyUsedCards(2);
				}
				else {
					player->setDamageReduction(damageReduction);
					player->setTheoreticallyUsedCards(1);
				}
			}
			else if (player->getCharacter() == BangCharacters::Jourdonnais) {
				if (1 < deck.size()) {
					if (deck[deck.size() - 2]->getColor() == CardColor::Heart)
						player->setDamageReduction(damageReduction + 1);
					else
						player->setDamageReduction(damageReduction);
				}
				else {
					std::default_random_engine e;
					std::uniform_int_distribution<unsigned> distribution(0, discardedCards.size() - 1);
					if(discardedCards[distribution(e)]->getColor() == CardColor::Heart)
						player->setDamageReduction(damageReduction + 1);
					else
						player->setDamageReduction(damageReduction);
				}
				player->setNumberOfCardsToBeDiscardedFromDeck(2);
			}
			else {
				player->setDamageReduction(damageReduction);
				player->setTheoreticallyUsedCards(1);
			}
		} 
		else if(player->getCharacter() == BangCharacters::Jourdonnais) {
			player->setDamageReduction(damageReduction);
			player->setTheoreticallyUsedCards(1);
		}
	}
}
void BangGame::discardCardsFromDeck(uint8_t numberOfCards)
{
	if (!(numberOfCards < this->numberOfCards))
		return;
	unsigned remainingCards = numberOfCards;
	if (deck.size() < numberOfCards) {
		remainingCards -= numberOfCards;
		discardedCards.insert(discardedCards.end(), std::make_move_iterator(deck.begin()),
			std::make_move_iterator(deck.end()));
		refreshDeck();
	}
	discardedCards.insert(discardedCards.end(), std::make_move_iterator(deck.rbegin()),
		std::make_move_iterator(deck.rbegin() + remainingCards));
	deck.erase(deck.begin() + (deck.size() - remainingCards - 1), deck.end());

}
void BangGame::distributeCardsForGeneralStore(uint8_t activePlayerIndex)
{
	std::vector<Card*> cardsToChooseFrom;
	if (deck.size() < numberOfPlayers) {
		cardsToChooseFrom.insert(cardsToChooseFrom.end(), std::make_move_iterator(deck.begin()),
			std::make_move_iterator(deck.end()));
		refreshDeck();
		cardsToChooseFrom.insert(cardsToChooseFrom.end(), std::make_move_iterator(deck.rbegin()),
			std::make_move_iterator(deck.rbegin() + (numberOfPlayers - cardsToChooseFrom.size())));
		deck.erase(deck.begin() + (deck.size() - cardsToChooseFrom.size() - 1), deck.end());
	}
	else {
		cardsToChooseFrom.insert(cardsToChooseFrom.end(), std::make_move_iterator(deck.rbegin()),
			std::make_move_iterator(deck.rbegin() + numberOfPlayers));
		deck.erase(deck.begin() + (deck.size() - cardsToChooseFrom.size() - 1), deck.end());
	}
	for (std::vector<BangPlayer*>::iterator player = players.begin() + activePlayerIndex; player != players.end(); ++player) {
		if (1 < cardsToChooseFrom.size()) {
			if (!(*player)->isComputer()) {
				std::cout << "\n" << (*player)->getPlayerName() << " , choose one of the following cards\n";
				for (size_t i = 0; i < cardsToChooseFrom.size(); ++i)
					std::cout << "Press " << i + 1 << " for " << cardsToChooseFrom[i]->getName() << "\n";
				unsigned choice;
				while (true) {
					std::cin >> choice;
					--choice;
					if (choice < cardsToChooseFrom.size()) {
						giveCardToPlayer(*player, cardsToChooseFrom[choice]);
						cardsToChooseFrom.erase(cardsToChooseFrom.begin() + choice);
						break;
					}
					else
						std::cout << "Invalid choice, choose one of the available cards\n";
				}
			}
			else {
				playersCopy = players;
				playersCopy2 = players;
				dynamic_cast<State*>((*player)->getCurrentState())->setOtherPlayers(getOtherPlayers());
				dynamic_cast<State*>((*player)->getCurrentState())->setInRangePlayers(getPlayersInRange());
				playersCopy = players;
				playersCopy2 = players;
				for (auto card : cardsToChooseFrom)
					(*player)->evaluateCard(card);
				CardInterface* chosenCard = (*player)->getCurrentBestPlay();
				(*player)->clearPossibleChoices();
				auto it = std::find(cardsToChooseFrom.begin(), cardsToChooseFrom.end(), chosenCard);
				if (it != cardsToChooseFrom.end()) {
					giveCardToPlayer(*player, *it);
					cardsToChooseFrom.erase(it);
				}
			}
		}
		else {
			giveCardToPlayer(*player, cardsToChooseFrom[0]);
			cardsToChooseFrom.clear();
		}
	}
	if (cardsToChooseFrom.size()) {
		for (std::vector<BangPlayer*>::iterator player = players.begin(); player != players.begin() + activePlayerIndex; ++player) {
			if (1 < cardsToChooseFrom.size()) {
				if (!(*player)->isComputer()) {
					std::cout << "\n" << (*player)->getPlayerName() << " , choose one of the following cards\n";
					for (size_t i = 0; i < cardsToChooseFrom.size(); ++i)
						std::cout << "Press " << i + 1 << " for " << cardsToChooseFrom[i]->getName() << "\n";
					unsigned choice;
					while (true) {
						std::cin >> choice;
						--choice;
						if (choice < cardsToChooseFrom.size()) {
							giveCardToPlayer(*player, cardsToChooseFrom[choice]);
							cardsToChooseFrom.erase(cardsToChooseFrom.begin() + choice);
							break;
						}
						else
							std::cout << "Invalid choice, choose one of the available cards\n";
					}
				}
				else {
					playersCopy = players;
					playersCopy2 = players;
					dynamic_cast<State*>((*player)->getCurrentState())->setOtherPlayers(getOtherPlayers());
					dynamic_cast<State*>((*player)->getCurrentState())->setInRangePlayers(getPlayersInRange());
					playersCopy = players;
					playersCopy2 = players;
					for (auto card : cardsToChooseFrom)
						(*player)->evaluateCard(card);
					CardInterface* chosenCard = (*player)->getCurrentBestPlay();
					(*player)->clearPossibleChoices();
					auto it = std::find(cardsToChooseFrom.begin(), cardsToChooseFrom.end(), chosenCard);
					giveCardToPlayer(*player, *it);
					cardsToChooseFrom.erase(it);
				}
			}
			else {
				giveCardToPlayer(*player, cardsToChooseFrom[0]);
				cardsToChooseFrom.clear();
			}
		}
	}
}
void BangGame::run()
{
	std::ofstream fout("log.txt", std::ios::app);
	//Logger logger(fout);
	//logger.log("Started the game");
	distributeRolesAndCharacters();
	displayRolesAndCharacters();
	setRelativeDistances();
	updatePlayerIndexMap();
	initializeDeck();
	distributeCards();
	gameState = GameState::NotFinished;
	uint64_t turn = 0;
	while (gameState == GameState::NotFinished) {
		//logger.log(std::to_string(deck.size()));
		activePlayerIndex=turn%numberOfPlayers;
		activePlayer = players[activePlayerIndex];
		std::cout << activePlayer->getPlayerName() << " has " << (int)activePlayer->getPlayerLifePoints() << " life points\n";
		activePlayer->setDynamite(passDynamiteToPlayer);
		playersCopy = players;
		playersCopy2 = players;
		if (activePlayer->getCardsToDiscard().size())
		{
			std::vector<CardInterface*> cardsToDiscard = activePlayer->getCardsToDiscard();
			for (CardInterface* discardedCard : cardsToDiscard)
				discardedCards.push_back(dynamic_cast<Card*>(discardedCard));
			//discardedCards.insert(discardedCards.end(), std::make_move_iterator(cardsToDiscard.begin()),
			//	std::make_move_iterator(cardsToDiscard.end()));
			cardsToDiscard.clear();
		}
		luckDrawPhase(activePlayer);
		if (!checkForActivePlayerDeath(activePlayer)) {
			if (!activePlayer->isInJail()) {
				bool playerTurnNotFinished = true;
				if (activePlayer->isComputer()) {
					dynamic_cast<State*>(activePlayer->getCurrentState())->setOtherPlayers(getOtherPlayers());
					dynamic_cast<State*>(activePlayer->getCurrentState())->setInRangePlayers(getPlayersInRange());
				}
				drawPhase(activePlayer);
				activePlayer->displayHand();
				if (!activePlayer->isComputer() && promptPlayerForEndOfTurn(activePlayer))
					playerTurnNotFinished = false;
				while (playerTurnNotFinished) {
					playersCopy = players;
					Card * chosenCard = nullptr;
					if (!activePlayer->isComputer())
						chosenCard = dynamic_cast<Card*>(activePlayer->playCard());
					else {
						activePlayer->evaluateCards();
						chosenCard = dynamic_cast<Card*>(activePlayer->getBestPlay());
					}
					if (chosenCard) {
						if (chosenCard->getTargetType() == CardTargetType::Targeted) {
							if (activePlayer->isComputer()) {
								Panic* panicCard = dynamic_cast<Panic*>(chosenCard);
								if (!panicCard)
									chosenCard->playCard(activePlayer, dynamic_cast<State*>(activePlayer->getCurrentState())->getTargetedPlayer());
								else {
									State* state = dynamic_cast<State*>(activePlayer->getCurrentState());
									panicCard->playCard(activePlayer, state->getTargetedPlayer(), state->getTargetedCard());
								}
							}
							else if (chosenCard->getRange() == CardRange::RangeDependent) {
								if (chosenCard->isDefendableFrom())
									checkForBarrels();
								auto range = getPlayersInRange();
								if (range.begin == range.end) {
									std::cout << "No players are in range\n";
									giveCardToPlayer(activePlayer, chosenCard);
								}
								else {
										chosenCard->playCard(activePlayer, range);
								}
							}
							else {
								BangPlayerIteratorRange r = getOtherPlayers();
								chosenCard->playCard(activePlayer, r);
							}
						}
						else {
							BangPlayerIteratorRange r = getOtherPlayers();
							chosenCard->playCard(activePlayer, r);
						}

						if (chosenCard->getCardState() == CardState::Unplayed) {
							giveCardToPlayer(activePlayer, chosenCard);
							if (promptPlayerForEndOfTurn(activePlayer))
								playerTurnNotFinished = false;
						}
						else {
							if (chosenCard->getType() == CardType::BlueBorderedCard) {
								if (!activePlayer->hasPlayedJail())
									activePlayer->addToActiveCards(chosenCard);
								else
									activePlayer->setPlayedJail(false);
							}
							else {
								if (activePlayer->hasPlayedGeneralStore()) {
									distributeCardsForGeneralStore(activePlayerIndex);
									activePlayer->setPlayedGeneralStore(false);
								}
								discardedCards.push_back(chosenCard);
							}
							if (activePlayer->getNumberOfCardsToDraw()) {
								giveCardsFromDeckToPlayer(activePlayer, activePlayer->getNumberOfCardsToDraw());
								activePlayer->resetNumberOfCardsToDraw();
								activePlayer->displayHand();
							}
							if (activePlayer->hasMustang() || activePlayer->hasScope())
								distanceAugmentations = true;
							if (!activePlayer->getNumberOfCards()) {
								if (activePlayer->getCharacter() == BangCharacters::SuzyLafayette) {
									giveCardsFromDeckToPlayer(activePlayer, 1);
									activePlayer->displayHand();
								}
								else
									playerTurnNotFinished = false;
							}
						}
						if (activePlayer->getCharacter() == BangCharacters::SidKetchum &&
							activePlayer->getPlayerLifePoints() < activePlayer->getPlayerMaxLifePoints()) {
							if (promptPlayerToDiscardCardsForLife(activePlayer)) {
								Card* chosenCard = dynamic_cast<Card*>(activePlayer->discardChosenCard());
								discardedCards.push_back(chosenCard);
								chosenCard = dynamic_cast<Card*>(activePlayer->discardChosenCard());
								discardedCards.push_back(chosenCard);
								activePlayer->increaseLifePoints();
							}
						}
						else {
							if (!activePlayer->isComputer() && promptPlayerForEndOfTurn(activePlayer))
								playerTurnNotFinished = false;
						}
					}
					else {
						playerTurnNotFinished = false;
					}
					if (activePlayer->needToRecalculateRelativeDistances()) {
						setRelativeDistances();
						activePlayer->setRecalculateRelativeDistances(false);
					}
					if (activePlayer->getNumberOfCardsToBeDiscardedFromDeck())
						discardCardsFromDeck(activePlayer->getNumberOfCardsToBeDiscardedFromDeck());
					if (checkForPlayerDeath(activePlayer))
						activePlayerIndex = turn % numberOfPlayers;
					if (activePlayer->isComputer()) {
						playersCopy = players;
						playersCopy2 = players;
						dynamic_cast<State*>(activePlayer->getCurrentState())->setOtherPlayers(getOtherPlayers());
						dynamic_cast<State*>(activePlayer->getCurrentState())->setInRangePlayers(getPlayersInRange());
						playersCopy = players;
						playersCopy2 = players;
					}
				}
				endTurn(activePlayer);
			}
		}
		++turn;
		//gameState = GameState::BanditVictory;
	}
}
