#pragma once

#include "BangGameEnums.h"
#include <string>

using namespace BangGameEnums;

class CardInterface {
public:
	virtual inline CardType getType() const= 0;
	virtual inline CardTargetType getTargetType() const = 0;
	virtual inline CardRange getRange() const = 0;
	virtual inline CardState getCardState() const = 0;
	virtual inline CardColor getColor() const = 0;
	virtual inline uint8_t getNumber() const = 0;
	virtual inline const std::string& getName() const = 0;
	virtual inline BangCard getBangCardType() const = 0;
};