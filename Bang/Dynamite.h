#pragma once
#include"Card.h"

class Dynamite :
	public Card
{
public:
	Dynamite(const std::string& name, CardColor color, uint8_t number);
	virtual void playCard(Player* player, PlayerIteratorRange & range) override final;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	void playCard(Player* player, Player* targetedPlayer) override;
	bool canBePlayed(Player* player) const override;
};

