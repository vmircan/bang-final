#include "GeneralStore.h"

GeneralStore::GeneralStore(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::GeneralStore, CardTargetType::Untargeted) {}

void GeneralStore::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	playCard(player, player);
}

void GeneralStore::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void GeneralStore::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << "\n" << player->getPlayerName() << " has played a " << name << "\n";
	player->setPlayedGeneralStore(true);
}

bool GeneralStore::canBePlayed(Player * player) const
{
	return true;
}
