#include "BangPlayerIteratorRange.h"

BangPlayer * BangPlayerIteratorRange::getPlayerTargetChoice(std::function<bool(Player*)> lamda)
{
	end = std::remove_if(begin, end, lamda);
	if (begin == end) {
		std::cout << "No players to target\n";
		return nullptr;
	}
	displayPlayerTargetingOptions();
	char choice;
	std::cin >> choice;
	return *(begin + ((unsigned)(choice - '0') - 1));
	return nullptr;
}

void BangPlayerIteratorRange::filterRange(std::function<bool(Player*)> lamda)
{
	end = std::remove_if(begin, end, lamda);
}

void BangPlayerIteratorRange::for_each(std::function<void (Player*)> lamda)
{
	std::for_each(begin, end, lamda);
}

void BangPlayerIteratorRange::clear()
{
	begin = end;
}

std::ostream & BangPlayerIteratorRange::displayPlayerTargetingOptions(std::ostream & os)
{
	unsigned counter = 0;
	std::for_each(begin, end, [&os, &counter](BangPlayer* player) {
		os << "Press " << ++counter << " for player " << player->getPlayerName() << "\n";
	});
	return os;
}
