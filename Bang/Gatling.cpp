#include "Gatling.h"

Gatling::Gatling(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Gatling, CardTargetType::Untargeted) {}

void Gatling::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << "\n";
	range.for_each([player](Player* targetedPlayer) {
		if (!targetedPlayer->decreaseLifePoints(DamageSource::Gatling)) {
			if (targetedPlayer->getCharacter() == BangCharacters::ElGringo) {
				if (player->getNumberOfCards()) {
					if (!player->isComputer())
						targetedPlayer->drawCard(player->discardRandomCard());
					else
						targetedPlayer->drawCard(player->decideOnDiscard());
					std::cout << targetedPlayer->getPlayerName() << " drew from " << player->getPlayerName() << "s hand\n";
				}
			}
		}
	});
}

void Gatling::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Gatling::playCard(Player * player, Player * targetedPlayer)
{
}

bool Gatling::canBePlayed(Player * player) const
{
	return true;
}
