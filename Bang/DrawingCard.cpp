#include "DrawingCard.h"
#include <iostream>
DrawingCard::DrawingCard(const std::string & name, CardColor color, uint8_t number, uint8_t numberOfCardToDraw) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::DrawingCard, CardTargetType::Untargeted)
{
	this->numberOfCardsToDraw = numberOfCardToDraw;
}

void DrawingCard::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	playCard(player, player);
}
uint8_t DrawingCard::getNumberOfCardsToDraw() const
{
	return numberOfCardsToDraw;
}

void DrawingCard::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void DrawingCard::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << "\n";
	for (size_t i = 0; i < numberOfCardsToDraw; ++i)
		player->increaseNumberOfCardsToDraw();
}

bool DrawingCard::canBePlayed(Player * player) const
{
	return true;
}
