#pragma once
#include"Card.h"

class Weapon:
	public Card
{
public:
	Weapon(const std::string& name, CardColor color, uint8_t number, uint8_t range, bool unlimitedBangs = false);
	virtual void playCard(Player* player, PlayerIteratorRange & range) override final;
	void playCard(Player* player, Player* targetedPlayer) override;
	uint8_t getWeaponRange() const;
	bool hasUnlimitedBangs() const;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	bool canBePlayed(Player* player) const override;
private:
	const uint8_t range;
	const bool unlimitedBangs;
};

