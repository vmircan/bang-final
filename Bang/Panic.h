#pragma once
#include"Card.h"

class Panic :
	public Card
{
public:
	Panic(const std::string& name, CardColor color, uint8_t number);
	virtual void playCard(Player* player, PlayerIteratorRange & range) override final;
	void filterTargetablePlayers(Player* player, PlayerIteratorRange& range) override;
	void playCard(Player* player, Player* targetedPlayer) override;
	void playCard(Player* player, Player* targetedPlayer, CardInterface* targetedCard);
	bool canBePlayed(Player* player) const override;
};

