#include "Barrel.h"

Barrel::Barrel(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BlueBorderedCard, BangCard::Barrel, CardTargetType::Untargeted) {}

void Barrel::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	if (canBePlayed(player))
		playCard(player, player);
	else {
		std::cout << "Can't play another " << name << "\n";
		state = CardState::Unplayed;
	}
}

void Barrel::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Barrel::playCard(Player * player, Player * targetedPlayer)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played a " << name << "\n";
	player->setBarrel(true);
}

bool Barrel::canBePlayed(Player * player) const
{
	return !player->hasBarrel();
}
