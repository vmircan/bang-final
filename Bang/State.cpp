#include "State.h"
#include "Bang.h"
#include "Barrel.h"
#include "Beer.h"
#include "CatBalou.h"
#include "DrawingCard.h"
#include "Duel.h"
#include "Dynamite.h"
#include "Gatling.h"
#include "GeneralStore.h"
#include "Indians.h"
#include "Jail.h"
#include "Missed.h"
#include "Mustang.h"
#include "Panic.h"
#include "Saloon.h"
#include "Scope.h"
#include "Weapon.h"

State::State(StateInterface * parentState, Player* player, Player* targetedPlayer,
	CardInterface* play, double gCost, double hCost, CardInterface* targetedCard) :
	parentState(parentState), player(player), targetedPlayer(targetedPlayer), play(play),
	gCost(gCost), hCost(hCost), targetedCard(targetedCard)
{

}

inline double State::getGCost() const
{
	return gCost;
}

inline double State::getHCost() const
{
	return hCost;
}

inline double State::getFCost() const
{
	return gCost + hCost;
}

CardInterface * State::getPlay() const
{
	return play;
}

Player * State::getPlayer() const
{
	return player;
}

Player * State::getTargetedPlayer()
{
	return targetedPlayer;
}

void State::setOtherPlayers(PlayerIteratorRange& otherPlayers)
{
	this->otherPlayers.clear();
	otherPlayers.for_each([this](Player* player) {this->addOtherPlayer(player); });
}

void State::setInRangePlayers(PlayerIteratorRange & inRangePlayers)
{
	this->inRangePlayers.clear();
	inRangePlayers.for_each([this](Player* player) {this->addInRangePlayer(player); });
}

const std::vector<Player*>& State::getOtherPlayers() const
{
	return otherPlayers;
}

const std::vector<Player*>& State::getInRangePlayers() const
{
	return inRangePlayers;
}

void State::addOtherPlayer(Player * otherPlayer)
{
	otherPlayers.push_back(otherPlayer);
}

void State::addInRangePlayer(Player * inRangePlayer)
{
	inRangePlayers.push_back(inRangePlayer);
}

CardInterface * State::getTargetedCard() const
{
	return targetedCard;
}

void State::setTargetedCard(CardInterface * targetedCard)
{
	this->targetedCard = targetedCard;
}

void State::calculateHCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange& playersInRange)
{
	CardInterface* play = currentState->getPlay();
}

void State::calculateUnreachableFCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange & playersInRange)
{
	calculateUnreachableGCost(currentState, otherPlayers, playersInRange);
	calculateUnreachableHCost(currentState, otherPlayers, playersInRange);
}

double State::calculateHCost(CardInterface * card) const
{
	return 0.0;
}

void State::calculateGCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange& playersInRange)
{
	CardInterface* play = currentState->getPlay();
	Weapon* weaponCard = dynamic_cast<Weapon*>(play);
	BangCard cardType = play->getBangCardType();
	Player* player = currentState->getPlayer();
	Player* targetedPlayer = currentState->getTargetedPlayer();
	uint8_t numberOfPlayers = 1;
	uint8_t numberOfOtherPlayersUnderMaxLifePoints=0;
	double otherPlayersAverageLifePoints;
	int otherPlayersTotalSumLifePoints = 0;
	otherPlayers.for_each([&otherPlayersTotalSumLifePoints, &numberOfOtherPlayersUnderMaxLifePoints, &numberOfPlayers](Player* player) {
		if (player->getPlayerLifePoints() < player->getPlayerMaxLifePoints())
			++numberOfOtherPlayersUnderMaxLifePoints;
		otherPlayersTotalSumLifePoints += player->getPlayerLifePoints();
		++numberOfPlayers;
	});
	std::cout << "\n---"<<(int)numberOfPlayers << "---\n";
	otherPlayersAverageLifePoints = otherPlayersTotalSumLifePoints / (numberOfPlayers-1);
	double gCost;
	player->calculateAverageHandValue();
	double averageHandValue = player->getAverageHandValue();
	int numberOfBangCards = 0, numberOfMissedCards = 0;
	for (auto card : player->getHand()) {
		Bang* bangCard = dynamic_cast<Bang*>(card);
		if (bangCard)
			++numberOfBangCards;
		Missed* missedCard = dynamic_cast<Missed*>(card);
		if (missedCard)
			++numberOfMissedCards;
	}
	switch (cardType)
	{
	case BangGameEnums::BangCard::Bang:
		gCost = 5 * targetedPlayer->getPlayerMaxLifePoints()/targetedPlayer->getPlayerLifePoints();
		if (targetedPlayer->getRole() == Roles::Sheriff) {
			if (player->getRole() == Roles::Deputie)
				gCost = 0;
			else
				gCost *= 2;
		}
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Barrel:
		gCost = 5 * player->getPlayerMaxLifePoints() / player->getPlayerLifePoints();
		if (player->getCharacter() == BangCharacters::LuckyDuke) {
			gCost *= 2;
		}
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Beer:
		gCost = 2 * (player->getPlayerMaxLifePoints() / player->getPlayerLifePoints()) *
			(player->getPlayerMaxLifePoints() / player->getPlayerLifePoints());
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::CatBalou:
		gCost = 2 * targetedPlayer->getPlayerMaxLifePoints() / targetedPlayer->getNumberOfCards();
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::DrawingCard:
		gCost = 10 * player->getPlayerLifePoints() / player->getNumberOfCards();
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Duel:
		gCost = 4 * targetedPlayer->getPlayerMaxLifePoints() / targetedPlayer->getPlayerLifePoints() * (numberOfBangCards/2);
		if (targetedPlayer->getRole() == Roles::Sheriff) {
			if (player->getRole() == Roles::Deputie)
				gCost = 0;
			else
				gCost *= 2;
		}
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Dynamite:
		gCost = 4 * numberOfPlayers * player->getPlayerLifePoints() / player->getPlayerMaxLifePoints();
		if(player->getCharacter()==BangCharacters::LuckyDuke)
			gCost*=gCost;
		if (player->getRole() == Roles::Renegade)
			gCost *= 2;
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Gatling:
		gCost = 5 * numberOfPlayers * 4 / otherPlayersAverageLifePoints;
		if (player->getRole() == Roles::Renegade)
			gCost *= 2;
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::GeneralStore:
		gCost = 4 * numberOfPlayers;
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Indians:
		gCost = 3 * numberOfPlayers * 4 / otherPlayersAverageLifePoints;
		if (player->getRole() == Roles::Renegade)
			gCost *= 2;
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Jail:
		gCost = 20 * 1/numberOfPlayers;
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Missed:
		currentState->setGCost(0);
		break;
	case BangGameEnums::BangCard::Mustang:
		gCost = 4 * numberOfPlayers * (player->getPlayerMaxLifePoints() / player->getPlayerLifePoints());
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Panic:
		gCost = 0;
		if(!currentState->getTargetedCard()){
			gCost = 4 * targetedPlayer->getPlayerMaxLifePoints() / (targetedPlayer->getNumberOfCards());
		}
		else {
			CardInterface* targetedCard = currentState->getTargetedCard();
			BangCard bangCardType = targetedCard->getBangCardType();
			switch (bangCardType)
			{
			case BangGameEnums::BangCard::Barrel:
				gCost = 5 * player->getPlayerMaxLifePoints() / player->getPlayerLifePoints();
				if (player->getCharacter() == BangCharacters::LuckyDuke) {
					gCost *= 2;
				}
				break;
			case BangGameEnums::BangCard::Jail:
				gCost = 20 * 1 / numberOfPlayers;
				break;
			case BangGameEnums::BangCard::Mustang:
				gCost = 4 * numberOfPlayers * (player->getPlayerMaxLifePoints() / player->getPlayerLifePoints());
				break;
			case BangGameEnums::BangCard::Scope:
				gCost = 10 * ((numberOfPlayers*0.5 + 1) / player->getPlayerRange());
				break;
			case BangGameEnums::BangCard::Weapon:
				Weapon* weaponCard = dynamic_cast<Weapon*>(targetedCard);
				gCost = 4 * (weaponCard->getWeaponRange() / player->getPlayerRange()) * 2 * numberOfPlayers*0.5;
				if (weaponCard->hasUnlimitedBangs() || player->getCharacter() == BangCharacters::WillyTheKid)
					gCost *= 2 * numberOfBangCards;
				break;
			}
			gCost *= 1.5;
		}
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Saloon:
		gCost = 1 * (player->getPlayerMaxLifePoints() - player->getPlayerLifePoints()) *
			(player->getPlayerMaxLifePoints() / player->getPlayerLifePoints()) * ((numberOfPlayers-1)/(numberOfOtherPlayersUnderMaxLifePoints+1));
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Scope:
		gCost = 10 * ((numberOfPlayers*0.5 + 1) / player->getPlayerRange());
		currentState->setGCost(gCost);
		break;
	case BangGameEnums::BangCard::Weapon:
		gCost = 4 * (weaponCard->getWeaponRange()/player->getPlayerRange())*2 * numberOfPlayers*0.5;
		if (weaponCard->hasUnlimitedBangs() || player->getCharacter()==BangCharacters::WillyTheKid)
			gCost *= 2 * numberOfBangCards;
		currentState->setGCost(gCost);
		break;
	}

}

void State::calculateFCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange & playersInRange)
{
	calculateGCost(currentState, otherPlayers, playersInRange);
	calculateHCost(currentState, otherPlayers, playersInRange);
}

void State::calculateUnreachableHCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange & playersInRange)
{
}

void State::calculateUnreachableGCost(State * currentState, PlayerIteratorRange & otherPlayers, PlayerIteratorRange & playersInRange)
{
	CardInterface* play = currentState->getPlay();
	BangCard cardType = play->getBangCardType();
	Player* player = currentState->getPlayer();
	Player* targetedPlayer = currentState->getTargetedPlayer();
	uint8_t numberOfPlayers = 1;
	uint8_t numberOfOtherPlayersUnderMaxLifePoints = 0;
	double otherPlayersAverageLifePoints;
	int otherPlayersTotalSumLifePoints = 0;
	otherPlayers.for_each([&otherPlayersTotalSumLifePoints, &numberOfOtherPlayersUnderMaxLifePoints, &numberOfPlayers](Player* player) {
		if (player->getPlayerLifePoints() < player->getPlayerMaxLifePoints())
			++numberOfOtherPlayersUnderMaxLifePoints;
		otherPlayersTotalSumLifePoints += player->getPlayerLifePoints();
		++numberOfPlayers;
	});
	std::cout << "\n+++" << (int)numberOfPlayers << "+++\n";
	otherPlayersAverageLifePoints = otherPlayersTotalSumLifePoints / numberOfPlayers;
	double gCost=0;
	double averageHandValue = player->getAverageHandValue();
	int numberOfBangCards = 0, numberOfMissedCards = 0;
	for (auto card : player->getHand()) {
		Bang* bangCard = dynamic_cast<Bang*>(card);
		if (bangCard)
			++numberOfBangCards;
		Missed* missedCard = dynamic_cast<Missed*>(card);
		if (missedCard)
			++numberOfMissedCards;
	}
	switch (cardType)
	{
	case BangGameEnums::BangCard::Bang:
		gCost = 10;
		break;
	case BangGameEnums::BangCard::Barrel:
		gCost = 2 * player->getPlayerMaxLifePoints() / player->getPlayerLifePoints();
		if (player->getCharacter() == BangCharacters::LuckyDuke) {
			gCost *= 2;
		}
		break;
	case BangGameEnums::BangCard::Beer:
		gCost = 15;
		break;
	case BangGameEnums::BangCard::CatBalou:
		gCost = 5;
		break;
	case BangGameEnums::BangCard::Jail:
		gCost = 4;
		break;
	case BangGameEnums::BangCard::Missed:
		gCost = 10;
		break;
	case BangGameEnums::BangCard::Mustang:
		gCost = 2;
		break;
	case BangGameEnums::BangCard::Panic:
		gCost = 7;
		break;
	case BangGameEnums::BangCard::Saloon:
		gCost = 20;
		break;
	case BangGameEnums::BangCard::Scope:
		gCost = 2;
		break;
	}
	currentState->setGCost(-gCost);
}

double State::calculateGCost(CardInterface * card) const
{
	BangCard cardType = card->getBangCardType();
	switch (cardType)
	{
	case BangGameEnums::BangCard::Bang:
		return 1;
	case BangGameEnums::BangCard::Barrel:
		return 2;
	case BangGameEnums::BangCard::Beer:
		return 3;
	case BangGameEnums::BangCard::CatBalou:
		return 4;
	case BangGameEnums::BangCard::DrawingCard:
		return 5;
	case BangGameEnums::BangCard::Duel:
		return 6;
	case BangGameEnums::BangCard::Dynamite:
		return 7;
	case BangGameEnums::BangCard::Gatling:
		return 8;
	case BangGameEnums::BangCard::GeneralStore:
		return 9;
	case BangGameEnums::BangCard::Indians:
		return 10;
	case BangGameEnums::BangCard::Jail:
		return 11;
	case BangGameEnums::BangCard::Missed:
		return 12;
	case BangGameEnums::BangCard::Mustang:
		return 13;
	case BangGameEnums::BangCard::Panic:
		return 14;
	case BangGameEnums::BangCard::Saloon:
		return 15;
	case BangGameEnums::BangCard::Scope:
		return 16;
	case BangGameEnums::BangCard::Weapon:
		return 17;
	default:
		return -1.0;
	}
}

double State::calculateFCost(CardInterface * card) const
{
	return calculateGCost(card) + calculateHCost(card);
}

void State::setGCost(double gCost)
{
	this->gCost = gCost;
}

void State::setHCost(double hCost)
{
	this->hCost = hCost;
}

