//#include "BangPlayerIteratorRange.h"
//#include <algorithm>
//#include <functional>
//
//class Temp {
//public:
//	static std::ostream & displayPlayerTargetingOptions(BangPlayerIteratorRange range, std::ostream & os=std::cout)
//	{
//		unsigned counter = 0;
//		std::for_each(range.begin, range.end, [&os, &counter](BangPlayer* player) {
//			os << "Press " << ++counter << " for player " << player->getPlayerName() << "\n";
//		});
//		return os;
//	}
//
//	static Player * getPlayerTargetChoice(BangPlayerIteratorRange range, std::function<bool(BangPlayer*)> lamda = [](BangPlayer*) {return false; })
//	{
//		range.end = std::remove_if(range.begin, range.end, lamda);
//		if (range.begin == range.end) {
//			std::cout << "No players to target\n";
//			return nullptr;
//		}
//		displayPlayerTargetingOptions(range);
//		char choice;
//		std::cin>>choice;
//		return *(range.begin + ((unsigned)(choice - '0') - 1));
//	}
//};