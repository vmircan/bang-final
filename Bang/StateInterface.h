#pragma once
#include "CardInterface.h"

class StateInterface
{
public:
	virtual double getGCost() const = 0;
	virtual double getHCost() const = 0;
	virtual double getFCost() const = 0;
	virtual double calculateHCost(CardInterface* currentState)const = 0;
	virtual double calculateGCost(CardInterface* card)const = 0;
	virtual double calculateFCost(CardInterface* card)const = 0;
	virtual void setGCost(double gCost) = 0;
	virtual void setHCost(double hCost) = 0;
};

