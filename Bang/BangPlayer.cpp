#include "Bang.h"
#include "Beer.h"
#include "BangPlayer.h"
#include "BangPlayerIteratorRange.h"
#include "Barrel.h"
#include "Jail.h"
#include "Mustang.h"
#include "Missed.h" 
#include "State.h"
#include "Scope.h"
#include "Weapon.h"
#include "Panic.h"

#include <chrono>
#include <iostream>
#include <random>

BangPlayer::BangPlayer(const std::string& playerName, Roles role, BangCharacters character, bool computer):
	playerName(playerName), role(role), character(character), computer(computer), range(1)
{
	if (character == BangCharacters::PaulRegret || character == BangCharacters::ElGringo)
		maxLifePoints = 3;
	else
		maxLifePoints = 4;
	if (role == Roles::Sheriff)
		++maxLifePoints;
	lifePoints = maxLifePoints;
	if (character == BangCharacters::WillyTheKid)
		bangsPerTurn = maxLifePoints + 5;
	else
		bangsPerTurn = 1;
	auto lamda = [](StateInterface* first, StateInterface* second) {
		return first->getFCost() < second->getFCost(); };
	possibleChoices = std::priority_queue < StateInterface*, std::vector<StateInterface*>, std::function<bool(StateInterface*, StateInterface*)>>(lamda);
	currentState = new State(nullptr, this, nullptr, nullptr, 0, 0);
}

BangPlayer::~BangPlayer() {

}

bool BangPlayer::operator==(const BangPlayer & other)
{
	return playerName==other.playerName;
}

bool BangPlayer::operator!=(const BangPlayer & other)
{
	return !(*this==other);
}

const std::string & BangPlayer::getPlayerName()
{
	return playerName;
}

Roles BangPlayer::getRole()
{
	return role;
}

bool BangPlayer::isComputer() const
{
	return computer;
}

BangCharacters BangPlayer::getCharacter() const
{
	return character;
}

void BangPlayer::drawCard(CardInterface * card)
{
	drawCards(card);
}

void BangPlayer::drawCards(CardInterface* card)
{
	std::cout << playerName << " drew a card\n";
	hand.insert(hand.end(), dynamic_cast<Card*>(card));
}

void BangPlayer::addToActiveCards(CardInterface * card)
{
	activeCards.push_back(card);
}

inline uint8_t BangPlayer::getPlayerLifePoints() const
{
	return lifePoints;
}

inline uint8_t BangPlayer::getPlayerMaxLifePoints() const
{
	return maxLifePoints;
}

inline uint8_t BangPlayer::getPlayerRange() const
{
	return range;
}

inline void BangPlayer::setPlayerRange(uint8_t range)
{
	this->range = range;
}

bool BangPlayer::decreaseLifePoints(DamageSource source)
{
	bool defended = false;
	if (lifePoints) {
		switch (source)
		{
		case BangGameEnums::DamageSource::Bang:
			if (damageReduction == 1) {
				if (character == BangCharacters::Jourdonnais && numberOfTheoreticallyUsedCards){
					if (numberOfTheoreticallyUsedCards == 1) {
						std::cout << "\n" << playerName << " has used his ability to dodge the Bang\n";
					}
					else {
						std::cout << "\n" << playerName << " has used a barrel to dodge the Bang\n";
						setBarrel(false);
					}
				}
				else {
					std::cout << "\n" << playerName << " has used a barrel to dodge the Bang\n";
					setBarrel(false);
				}
				defended = true;
				numberOfCardsToBeDiscardedFromDeck = numberOfTheoreticallyUsedCards;
			}
			else if (damageReduction == 2) {
				std::cout << "\n" << playerName << " has used his ability to dodge the Bang\n";
				numberOfCardsToBeDiscardedFromDeck = 1;
			}
			else
				defended = defend(1);
			break;
		case BangGameEnums::DamageSource::Duel:
			break;
		case BangGameEnums::DamageSource::Gatling:
			defended = defend(1);
			break;
		case BangGameEnums::DamageSource::Indians:
			break;
		case BangGameEnums::DamageSource::SlabTheKiller:
			if (damageReduction == 1) {
				if (character == BangCharacters::Jourdonnais) {
					defended = defend(1);
					if (numberOfTheoreticallyUsedCards == 1) {
						if(defended)
							std::cout << "\n" << playerName << " has used his ability and a Missed to dodge the Bang\n";
					}
					else {
						if(defended)
							std::cout << "\n" << playerName << " has used a Barrel and a Missed to dodge the Bang\n";
						else
							std::cout << "\n" << playerName << " has used his Barrel\n";
						setBarrel(false);
					}
				}
				else {
					std::cout << "\n" << playerName << " has used a Barrel\n";
					defended = defend(1);
					setBarrel(false);
				}
				numberOfCardsToBeDiscardedFromDeck = numberOfTheoreticallyUsedCards;
			}
			else if (damageReduction == 2) {
				std::cout << "\n" << playerName << " has used his ability and a barrel to dodge the Bang\n";
				barrel = false;
				numberOfCardsToBeDiscardedFromDeck = 2;
				defended = true;
			}
			else
				defended = defend(2);
			break;
		default:
			break;
		}
		if (!defended) {
			if (source != DamageSource::Dynamite) {
				std::cout << playerName << " has lost a life point\n";
			}
			--lifePoints;
			if (character == BangCharacters::BartCassidy)
				++numberOfCardsToDraw;
			if (!lifePoints) {
				for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end();) {
					Beer* beerCard = dynamic_cast<Beer*>(*it);
					if (beerCard) {
						std::cout << playerName << " has used a " << beerCard->getName() << "\n";
						increaseLifePoints();
						it=hand.erase(it);
						break;
					}
					else
						++it;
				}
			}
			return false;
		}
		return true;
	}
	return false;
}

inline bool BangPlayer::increaseLifePoints()
{
	if (lifePoints < maxLifePoints) {
		std::cout<<playerName<<" has increased his life points to "<<(int)++lifePoints<<"\n";
		return true;
	}
	std::cout << "Can't increase "<<playerName<<"s life points above max life points\n";
	return false;
}

void BangPlayer::discardAllCards()
{
	cardsToDiscard.insert(cardsToDiscard.end(), std::make_move_iterator(hand.begin()), std::make_move_iterator(hand.end()));
	cardsToDiscard.insert(cardsToDiscard.end(), std::make_move_iterator(activeCards.begin()), std::make_move_iterator(activeCards.end()));
	hand.clear();
	activeCards.clear();
}

CardInterface* BangPlayer::discardChosenCard()
{
	if (!hand.size()) 
		return nullptr;
	std::cout << playerName<<", choose a card to discard:\n";
	unsigned cardChoice = getCardChoice();
	if (!(cardChoice < hand.size()))
		return nullptr;
	CardInterface* temp = hand[cardChoice];
	hand.erase(hand.begin() + cardChoice);
	return temp;
}

CardInterface * BangPlayer::discardCard()
{
	if(!hand.size())
		return nullptr;
	CardInterface* temp = hand.back();
	hand.pop_back();
	return temp;
}

CardInterface * BangPlayer::discardRandomCard()
{
	if (hand.size()) {
		std::default_random_engine engine;
		//engine.seed(std::chrono::system_clock::now().time_since_epoch().count);
		std::uniform_int_distribution<size_t> distribution(0, hand.size() - 1);
		size_t randomIndex = distribution(engine);
		CardInterface* temp = hand[randomIndex];
		hand.erase(hand.begin() + randomIndex);
		cardsToDiscard.push_back(temp);
		return temp;
	}
	else
		return nullptr;
}

CardInterface * BangPlayer::discardActiveCard(uint8_t index)
{
	CardInterface* temp = activeCards[index];
	activeCards.erase(activeCards.begin() + index);
	Weapon* weaponCard = dynamic_cast<Weapon*>(temp);
	Jail* jailCard = dynamic_cast<Jail*>(temp);
	Barrel* barrelCard = dynamic_cast<Barrel*>(temp);
	Mustang* mustangCard = dynamic_cast<Mustang*>(temp);
	Scope* scopeCard = dynamic_cast<Scope*>(temp);
	if (weaponCard)
		range = 1;
	else if (jailCard)
		jail = false;
	else if (barrelCard)
		barrel = false;
	else if (mustangCard) {
		mustang = false;
		recalculateRelativeDistances = true;
	}
	else if (scopeCard) {
		scope = false;
		recalculateRelativeDistances = true;
	}
	cardsToDiscard.push_back(temp);
	return temp;
}

CardInterface * BangPlayer::discardActiveCard(CardInterface * cardToDiscard)
{
	return discardActiveCard(std::find(activeCards.begin(), activeCards.end(), cardToDiscard) - activeCards.begin());
}

inline uint8_t BangPlayer::getNumberOfBangsPlayed() const
{
	return bangsPlayedThisTurn;
}

inline uint8_t BangPlayer::getNumberOfBangsPerTurn() const
{
	return bangsPerTurn;
}

void BangPlayer::resetBangsPlayed()
{
	bangsPlayedThisTurn = 0;
}

unsigned BangPlayer::getNumberOfCards() const
{
	return hand.size();
}

void BangPlayer::resetNumberOfCardsToDraw()
{
	numberOfCardsToDraw = 0;
}

uint8_t BangPlayer::getNumberOfCardsToDraw()
{
	return numberOfCardsToDraw;
}

inline void BangPlayer::increaseNumberOfCardsToDraw()
{
	++numberOfCardsToDraw;
}

void BangPlayer::putWeaponInPlay(CardInterface * weapon)
{
	for (std::vector<CardInterface*>::iterator it = activeCards.begin(); it != activeCards.end();) {
		Weapon* weaponCard = dynamic_cast<Weapon*>(*it);
		if (weaponCard) {
			cardsToDiscard.push_back(weaponCard);
			it=activeCards.erase(it);
			break;
		}
		else
			++it;
	}
}

inline void BangPlayer::setUnlimitedBangs(bool unlimitedBangs)
{
	if (unlimitedBangs)
		bangsPerTurn = 100;
	else
		bangsPerTurn = 1;
}

void BangPlayer::displayHand()
{
	std::cout << "\n"<<playerName<<"s hand:\n";
	std::for_each(hand.begin(), hand.end(), [](CardInterface* card) {
		std::cout << card->getName() << " ";
	});
	std::cout << "\n";
}

void BangPlayer::displayActiveCards()
{
	std::cout << "\n"<<playerName << "s active cards:\n";
	std::for_each(hand.begin(), hand.end(), [](CardInterface* card) {
		std::cout << card->getName() << ", ";
	});
	std::cout << "\n";
}

inline bool BangPlayer::hasDynamite() const
{
	return dynamite;
}

inline void BangPlayer::setDynamite(bool dynamite)
{
	this->dynamite = dynamite;
}

inline void BangPlayer::increaseBangsPlayed()
{
	++bangsPlayedThisTurn;
}

unsigned BangPlayer::getCardChoice()
{
	unsigned choice;
	while (true) {
		for (size_t i = 0; i < hand.size(); ++i)
			std::cout << "Press " << i + 1 << " for " << hand[i]->getName() << "\n";
		std::cin >> choice;
		if (choice - 1 < hand.size())
			break;
		else
			std::cout << "Invalid choice, choose an index among those specified\n";
	}
	return choice-1;
}

unsigned BangPlayer::getTargetChoice()
{
	char choice;
	std::cin>>choice;
	return unsigned(choice - '0') - 1;
}

inline bool BangPlayer::hasMustang() const
{
	return mustang;
}

inline void BangPlayer::setMustang(bool mustang)
{
	this->mustang = mustang;
}

inline bool BangPlayer::isInJail() const
{
	return jail;
}

inline void BangPlayer::putInJail()
{
	jail = true;
}

inline void BangPlayer::getOutOfJail()
{
	for (std::vector<CardInterface*>::iterator it = activeCards.begin(); it != activeCards.end();) {
		Jail* jailCard = dynamic_cast<Jail*>(*it);
		if (jailCard) {
			cardsToDiscard.push_back(jailCard);
			it = activeCards.erase(it);
			break;
		}
		else
			++it;
	}
	jail = false;
}

inline bool BangPlayer::hasScope() const
{
	return scope;
}

inline void BangPlayer::setScope(bool scope)
{
	this->scope = scope;
}

bool BangPlayer::defend(uint8_t defendRequirement)
{
	if (defendRequirement == 1) {
		for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end();) {
			Missed* missedCard = dynamic_cast<Missed*>(*it);
			if (missedCard) {
				bool defended = false;
				if (!computer) {
					std::cout << playerName << ", use a " << missedCard->getName() << " to defend? (Y/N)\n";
					defended = getPlayerChoice();
				}
				else
					defended = decideOnDefend();
				if (defended) {
					it = hand.erase(it);
					cardsToDiscard.push_back(missedCard);
				}
				return defended;

			}
			else if (character == BangCharacters::CalamityJones) {
				Bang* bangCard = dynamic_cast<Bang*>(*it);
				if (bangCard) {
					bool defended = false;
					if (!computer) {
						std::cout << playerName << ", use a " << bangCard->getName() << " to defend? (Y/N)\n";
						defended = getPlayerChoice();
					}
					else
						defended = decideOnDefend();
					if (defended) {
						it=hand.erase(it);
						cardsToDiscard.push_back(bangCard);
					}
					return defended;
				}
				else
					++it;
			}
			else
				++it;
		}
	}
	else {
		if (character != BangCharacters::CalamityJones) {
			unsigned missedCardNumber = 0;
			std::vector<CardInterface*>::iterator firstMissedCard, secondMissedCard;
			for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end(); ++it) {
				Missed* missedCard = dynamic_cast<Missed*>(*it);
				if (missedCard) {
					if (++missedCardNumber == 1)
						firstMissedCard = it;
					else {
						secondMissedCard = it;
						break;
					}
				}
			}
			if (missedCardNumber == 2) {
				bool defended = false;
				if (!computer) {
					std::cout <<playerName<< ", use two " << (*firstMissedCard)->getName() << " cards to defend? (Y/N)\n";
					defended = getPlayerChoice();
				}
				else
					defended = decideOnDefend();
				if (defended) {
					cardsToDiscard.push_back(*firstMissedCard);
					cardsToDiscard.push_back(*secondMissedCard);
					CardInterface* card = *secondMissedCard;
					hand.erase(firstMissedCard);
					hand.erase(std::find(hand.begin(), hand.end(), card));
				}
				return defended;
			}
		}
		else {
			unsigned missedCardNumber = 0, bangCardNumber = 0;;
			for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end(); ++it) {
				Missed* missedCard = dynamic_cast<Missed*>(*it);
				if (missedCard)
					++missedCardNumber;
				Bang* bangCard = dynamic_cast<Bang*>(*it);
				if (bangCard)
					++bangCardNumber;
			}
			if ((missedCardNumber + bangCardNumber) < 2)
				return false;
			unsigned optionCounter=0;
			std::vector<std::pair<uint8_t, uint8_t>> possibleChoices;
			unsigned option;
			if (!computer) {
				std::cout << playerName << " defend?\n";
				if(!getPlayerChoice())
					return false;
				std::cout << playerName << ", choose from the following defending options\n";
			}
			int defendChoice;
			if (!computer) {
				do {
					optionCounter = 0;
					possibleChoices.clear();
					if (1 < bangCardNumber) {
						std::cout << "Press " << ++optionCounter << " to use " << " two Bang cards\n";
						possibleChoices.emplace_back(0, 2);
					}
					if (0 < missedCardNumber && 0 < bangCardNumber) {
						std::cout << "Press " << ++optionCounter << " to use " << " one Missed and one Bang\n";
						possibleChoices.emplace_back(1, 1);
					}
					if (1 < missedCardNumber) {
						std::cout << "Press " << ++optionCounter << " to use " << " two Missed cards\n";
						possibleChoices.emplace_back(2, 0);
					}
					std::cin >> option;
					if (possibleChoices.size() < option)
						std::cout << "Invalid choice\n";
				} while (possibleChoices.size() < option);
			}
			else if (computer) {
				defendChoice = decideOnDefend(possibleChoices);
				if (defendChoice == -1)
					return false;
			}
			std::pair<uint8_t, uint8_t> chosenPair;
			if (!computer)
				chosenPair = possibleChoices[option-1];
			else
				chosenPair = possibleChoices[defendChoice];
			unsigned numberOfMissedPlayed = chosenPair.first, numberOfBangsPlayed = chosenPair.second;
			for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end() && (numberOfMissedPlayed + numberOfBangsPlayed);) {
				Missed* missedCard = dynamic_cast<Missed*>(*it);
				Bang* bangCard = dynamic_cast<Bang*>(*it);
				if (missedCard && numberOfMissedPlayed) {
					--numberOfMissedPlayed;
					cardsToDiscard.push_back(missedCard);
					it=hand.erase(it);
				}
				else if (bangCard && numberOfBangsPlayed) {
					--numberOfBangsPlayed;
					cardsToDiscard.push_back(bangCard);
					it=hand.erase(it);
				}
				else
					++it;
			}
			return true;
		}
	}
	return false;
}

inline bool BangPlayer::getPlayerChoice()
{
	char choice;
	std::cin>>choice;
	if (tolower(choice) == 'y')
		return true;
	return false;
}

bool BangPlayer::needToRecalculateRelativeDistances() const
{
	return recalculateRelativeDistances;
}

void BangPlayer::setRecalculateRelativeDistances(bool recalculate)
{
	recalculateRelativeDistances = recalculate;
}

bool BangPlayer::useBang()
{
	for (std::vector<CardInterface*>::iterator it = hand.begin(); it != hand.end();) {
		Bang* bangCard = dynamic_cast<Bang*>(*it);
		if (bangCard) {
			std::cout << playerName<< ", use a " << bangCard->getName() << " to defend? (Y/N)\n";
			if (getPlayerChoice()) {
				it=hand.erase(it);
				cardsToDiscard.push_back(bangCard);
				return true;
			}
			return false;
		}
		++it;
	}
	return false;
}

inline uint8_t BangPlayer::getNumberOfActiveCards() const
{
	return static_cast<uint8_t>(activeCards.size());
}

inline void BangPlayer::setBarrel(bool barrel)
{
	if(barrel)
		this->barrel = true;
	else {
		for (std::vector<CardInterface*>::iterator it = activeCards.begin(); it != activeCards.end();) {
			Barrel* barrelCard = dynamic_cast<Barrel*>(*it);
			if (barrelCard) {
				cardsToDiscard.push_back(barrelCard);
				it = activeCards.erase(it);
				break;
			}
			else
				++it;
		}
		this->barrel = false;
	}
}

inline bool BangPlayer::hasBarrel() const
{
	return barrel;
}

void BangPlayer::setDamageReduction(uint8_t damageReduction)
{
	this->damageReduction = damageReduction;
}

inline void BangPlayer::setNumberOfCardsToBeDiscardedFromDeck(uint8_t numberOfCardsToDiscard)
{
	numberOfCardsToBeDiscardedFromDeck = numberOfCardsToDiscard;
}

inline uint8_t BangPlayer::getNumberOfCardsToBeDiscardedFromDeck() const
{
	return numberOfCardsToBeDiscardedFromDeck;
}

void BangPlayer::setTheoreticallyUsedCards(uint8_t numberOfCards)
{
	numberOfTheoreticallyUsedCards = numberOfCards;
}

inline void BangPlayer::setPlayedGeneralStore(bool played)
{
	playedGeneralStore = played;
}

inline bool BangPlayer::hasPlayedGeneralStore() const
{
	return playedGeneralStore;
}

inline void BangPlayer::setPlayedJail(bool played)
{
	playedJail = true;
}

inline bool BangPlayer::hasPlayedJail() const
{
	return playedJail;
}

const std::vector<CardInterface*>& BangPlayer::getActiveCards() const
{
	return activeCards;
}

auto & BangPlayer::getPossibleChoices() const
{
	return possibleChoices;
}

CardInterface * BangPlayer::decideOnDiscard()
{
	if (!hand.size())
		return nullptr;
	for (auto card : hand)
		evaluateCardReverseCost(card);
	Card* card = dynamic_cast<Card*>(getCurrentBestPlay());
	auto lamda = [](StateInterface* first, StateInterface* second) {
		return first->getFCost() < second->getFCost(); };
	possibleChoices = std::priority_queue < StateInterface*, std::vector<StateInterface*>, std::function<bool(StateInterface*, StateInterface*)>>(lamda);
	if (!card)
		return card;
	auto it = std::find(hand.begin(), hand.end(), card);
	if (it != hand.end())
		hand.erase(it);
	else
		return nullptr;
	cardsToDiscard.push_back(card);
	return card;
}

bool BangPlayer::decideOnDefend() const
{
	return true;
}

int BangPlayer::decideOnDefend(const std::vector<std::pair<uint8_t, uint8_t>>& possibleDefendingOptions) const
{
	return 0;
}

bool BangPlayer::decideOnBangUse()
{
	for (auto it = hand.begin(); it != hand.end();) {
		Bang* bangCard = dynamic_cast<Bang*>(*it);
		if (bangCard) {
			it = hand.erase(it);
			cardsToDiscard.push_back(bangCard);
			return true;
		}
		++it;
	}
	return false;
}

bool BangPlayer::decideOnDiscardingCardsForLife() const
{
	return false;
}

Player * BangPlayer::decideOnDrawingFromAnotherPlayer() const
{
	for (Player* player : dynamic_cast<State*>(currentState)->getOtherPlayers()) {
		if (!(player == this) && player->getNumberOfCards())
			return player;
	}
	return nullptr;
}

void BangPlayer::evaluateCard(CardInterface* card) {
	State* currentState = dynamic_cast<State*>(this->currentState);
	std::vector<BangPlayer*> otherPlayersVector;
	std::vector<BangPlayer*> inRangePlayersVector;
	std::transform(currentState->getOtherPlayers().begin(), currentState->getOtherPlayers().end(),
		std::back_inserter(otherPlayersVector), [](Player* player) {return dynamic_cast<BangPlayer*>(player); });
	std::transform(currentState->getInRangePlayers().begin(), currentState->getInRangePlayers().end(),
		std::back_inserter(inRangePlayersVector), [](Player* player) {return dynamic_cast<BangPlayer*>(player); });

	double totalCardValue = 0;
	uint8_t numberOfPossibilities=0;
	std::vector<BangPlayer*> otherPlayersVector2(otherPlayersVector);
	BangPlayerIteratorRange otherPlayers(otherPlayersVector.begin(), otherPlayersVector.end());
	BangPlayerIteratorRange otherPlayersCopy(otherPlayersVector2.begin(), otherPlayersVector2.end());
	BangPlayerIteratorRange inRangePlayers(inRangePlayersVector.begin(), inRangePlayersVector.end());
	std::vector<BangPlayer*> inRangePlayersVector2(inRangePlayersVector);
	BangPlayerIteratorRange inRangePlayersCopy(inRangePlayersVector2.begin(), inRangePlayersVector2.end());
	Card* card2 = dynamic_cast<Card*>(card);
	if (card->getTargetType() == CardTargetType::Targeted) {
		if (card->getRange() == CardRange::RangeDependent) {
			card2->filterTargetablePlayers(this, inRangePlayers);
			if (card2->getBangCardType() != BangCard::Panic) {
				inRangePlayers.for_each([this, &totalCardValue, &numberOfPossibilities, card, &otherPlayers, &inRangePlayers](Player* player) {
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateFCost(newState, otherPlayers, inRangePlayers);
					this->possibleChoices.push(newState);
					totalCardValue += newState->getFCost();
					++numberOfPossibilities;
				});
			}
			else {
				inRangePlayers.for_each([this, card, &otherPlayers,&numberOfPossibilities, &totalCardValue, &inRangePlayersCopy](Player* player) {
					for (auto activeCard : player->getActiveCards()) {
						State* newState = new State(this->getCurrentState(), this, player, card, 0, 0, activeCard);
						State::calculateFCost(newState, otherPlayers, inRangePlayersCopy);
						this->possibleChoices.push(newState);
						totalCardValue += newState->getFCost();
						++numberOfPossibilities;
					}
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateFCost(newState, otherPlayers, inRangePlayersCopy);
					this->possibleChoices.push(newState);
					totalCardValue += newState->getFCost();
					++numberOfPossibilities;
				});
			}
		}
		else {
			Card* card2 = dynamic_cast<Card*>(card);
			card2->filterTargetablePlayers(this, otherPlayersCopy);
			otherPlayersCopy.for_each([this, card, &otherPlayers, &otherPlayersCopy, &numberOfPossibilities, &totalCardValue](Player* player) {
				State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
				State::calculateFCost(newState, otherPlayers, otherPlayersCopy);
				this->possibleChoices.push(newState);
				totalCardValue += newState->getFCost();
				++numberOfPossibilities;
			});
		}
	}
	else if(card2->canBePlayed(currentState->getPlayer())){
		State* newState = new State(this->getCurrentState(), this, this, card, 0, 0);
		State::calculateFCost(newState, otherPlayers, inRangePlayers);
		lastInsertedState = newState;
		possibleChoices.push(newState);
		totalCardValue += newState->getFCost();
		++numberOfPossibilities;
	}
	currentState->getPlayer()->addToTotalCost(totalCardValue / numberOfPossibilities);

}

void BangPlayer::evaluateCardReverseCost(CardInterface * card)
{
	State* currentState = dynamic_cast<State*>(this->currentState);
	std::vector<BangPlayer*> otherPlayersVector;
	std::vector<BangPlayer*> inRangePlayersVector;
	std::transform(currentState->getOtherPlayers().begin(), currentState->getOtherPlayers().end(),
		std::back_inserter(otherPlayersVector), [](Player* player) {return dynamic_cast<BangPlayer*>(player); });
	std::transform(currentState->getInRangePlayers().begin(), currentState->getInRangePlayers().end(),
		std::back_inserter(inRangePlayersVector), [](Player* player) {return dynamic_cast<BangPlayer*>(player); });

	std::vector<BangPlayer*> otherPlayersVector2(otherPlayersVector);
	std::vector<BangPlayer*> inRangePlayersVector2(inRangePlayersVector);
	BangPlayerIteratorRange otherPlayers(otherPlayersVector.begin(), otherPlayersVector.end());
	BangPlayerIteratorRange otherPlayersCopy(otherPlayersVector2.begin(), otherPlayersVector2.end());
	BangPlayerIteratorRange inRangePlayers(inRangePlayersVector.begin(), inRangePlayersVector.end());
	BangPlayerIteratorRange inRangePlayersCopy(inRangePlayersVector2.begin(), inRangePlayersVector2.end());
	Card* card2 = dynamic_cast<Card*>(card);
	if (card->getTargetType() == CardTargetType::Targeted) {
		if (card->getRange() == CardRange::RangeDependent) {
			card2->filterTargetablePlayers(this, inRangePlayers);
			std::vector<BangPlayer*> outOfRangePlayersVector;
			std::copy_if(otherPlayersVector.cbegin(), otherPlayersVector.cend(),
				std::back_inserter(outOfRangePlayersVector), [&inRangePlayersVector2](Player* player)
			{return std::find(inRangePlayersVector2.cbegin(), inRangePlayersVector2.cend(), player)==inRangePlayersVector2.cend(); });
			BangPlayerIteratorRange outOfRangePlayers(outOfRangePlayersVector.begin(), outOfRangePlayersVector.end());
			if (card2->getBangCardType() != BangCard::Panic) {
				inRangePlayers.for_each([this, card, &otherPlayers, &inRangePlayers](Player* player) {
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateFCost(newState, otherPlayers, inRangePlayers);
					newState->setGCost(-newState->getGCost());
					newState->setHCost(-newState->getHCost());
					this->possibleChoices.push(newState);
				});
				outOfRangePlayers.for_each([this, card, &otherPlayers, &inRangePlayersCopy](Player* player) {
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateUnreachableFCost(newState, otherPlayers, inRangePlayersCopy);
					newState->setGCost(-newState->getGCost());
					newState->setHCost(-newState->getHCost());
					this->possibleChoices.push(newState);
				});
			}
			else {
				inRangePlayers.for_each([this, card, &otherPlayers, &inRangePlayersCopy](Player* player) {
					for (auto activeCard : player->getActiveCards()) {
						State* newState = new State(this->getCurrentState(), this, player, card, 0, 0, activeCard);
						State::calculateFCost(newState, otherPlayers, inRangePlayersCopy);
						newState->setGCost(-newState->getGCost());
						newState->setHCost(-newState->getHCost());
						this->possibleChoices.push(newState);
					}
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateFCost(newState, otherPlayers, inRangePlayersCopy);
					newState->setGCost(-newState->getGCost());
					newState->setHCost(-newState->getHCost());
					this->possibleChoices.push(newState);
				});
				outOfRangePlayers.for_each([this, card, &otherPlayers, &inRangePlayersCopy](Player* player) {
					for (auto activeCard : player->getActiveCards()) {
						State* newState = new State(this->getCurrentState(), this, player, card, 0, 0, activeCard);
						State::calculateUnreachableFCost(newState, otherPlayers, inRangePlayersCopy);
						newState->setGCost(-newState->getGCost());
						newState->setHCost(-newState->getHCost());
						this->possibleChoices.push(newState);
					}
					State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
					State::calculateUnreachableFCost(newState, otherPlayers, inRangePlayersCopy);
					newState->setGCost(-newState->getGCost());
					newState->setHCost(-newState->getHCost());
					this->possibleChoices.push(newState);
				});
			}
		}
		else {
			Card* card2 = dynamic_cast<Card*>(card);
			card2->filterTargetablePlayers(this, otherPlayersCopy);
			std::vector<BangPlayer*> untargetablePlayersVector;
			std::copy_if(otherPlayersVector2.cbegin(), otherPlayersVector2.cend(),
				std::back_inserter(untargetablePlayersVector), [&otherPlayersCopy](Player* player)
			{return std::find(otherPlayersCopy.begin, otherPlayersCopy.end, player) == otherPlayersCopy.end; });
			BangPlayerIteratorRange untargetablePlayers(untargetablePlayersVector.begin(), untargetablePlayersVector.end());
			otherPlayersCopy.for_each([this, card, &otherPlayers, &otherPlayersCopy](Player* player) {
				State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
				State::calculateFCost(newState, otherPlayers, otherPlayersCopy);
				newState->setGCost(-newState->getGCost());
				newState->setHCost(-newState->getHCost());
				this->possibleChoices.push(newState);
			});
			untargetablePlayers.for_each([this, card, &otherPlayers, &otherPlayersCopy](Player* player) {
				State* newState = new State(this->getCurrentState(), this, player, card, 0, 0);
				State::calculateUnreachableFCost(newState, otherPlayers, otherPlayersCopy);
				newState->setGCost(-newState->getGCost());
				newState->setHCost(-newState->getHCost());
				this->possibleChoices.push(newState);
			});
		}
	}
	else {
		if (card2->canBePlayed(currentState->getPlayer())) {
			State* newState = new State(this->getCurrentState(), this, this, card, 0, 0);
			State::calculateFCost(newState, otherPlayers, inRangePlayers);
			newState->setGCost(-newState->getGCost());
			newState->setHCost(-newState->getHCost());
			lastInsertedState = newState;
			possibleChoices.push(newState);
		}
		else {
			State* newState = new State(this->getCurrentState(), this, this, card, 0, 0);
			State::calculateUnreachableFCost(newState, otherPlayers, inRangePlayers);
			newState->setGCost(-newState->getGCost());
			newState->setHCost(-newState->getHCost());
			lastInsertedState = newState;
			possibleChoices.push(newState);
		}
	}
}

void BangPlayer::evaluateCards(CardInterface * card)
{
	evaluateCard(card);
}

void BangPlayer::evaluateCardsReverseCost(CardInterface * card)
{
	evaluateCardReverseCost(card);
}

double BangPlayer::getKeepabilityTreshold() const
{
	return keepabilityTreshold;
}

std::vector<CardInterface*>& BangPlayer::getCardsToDiscard()
{
	return cardsToDiscard;
}

const std::vector<CardInterface*> BangPlayer::getHand() const
{
	return hand;
}

CardInterface* BangPlayer::playCard() {
	if (!hand.size())
		return nullptr;
	bool playedValidCard = false;
	unsigned cardChoice;
	CardInterface* chosenCard = nullptr;
	while (!playedValidCard) {
		playedValidCard = true;
		cardChoice = getCardChoice();
		if (!(cardChoice < hand.size())) {
			std::cout << "Invalid choice : Choose an index among those specified\n";
			playedValidCard = false;
		}
		else {
			chosenCard = hand[cardChoice];
			Missed* missedCard = dynamic_cast<Missed*>(chosenCard);
			if (missedCard) {
				if (character != BangCharacters::CalamityJones) {
					std::cout << "Invalid choice : Can't play a " << missedCard->getName() << " on your turn\n";
					playedValidCard = false;
				}
			}
		}
	}
	hand.erase(hand.begin() + cardChoice);
	return chosenCard;
}

void BangPlayer::clearPossibleChoices()
{
	auto lamda = [](StateInterface* first, StateInterface* second) {
		return first->getFCost() < second->getFCost(); };
	possibleChoices = std::priority_queue < StateInterface*, std::vector<StateInterface*>, std::function<bool(StateInterface*, StateInterface*)>>(lamda);
	totalHandValue = 0;
	numberOfEvaluatedCards = 0;
}

StateInterface* BangPlayer::getBestChoice() {
	StateInterface* choice = nullptr;
	if (possibleChoices.size()) {
		choice = possibleChoices.top();
		auto lamda = [](StateInterface* first, StateInterface* second) {
			return first->getFCost() < second->getFCost(); };
		possibleChoices = std::priority_queue < StateInterface*, std::vector<StateInterface*>, std::function<bool(StateInterface*, StateInterface*)>>(lamda);
		currentState = choice;
		totalHandValue = 0;
		numberOfEvaluatedCards = 0;
	}
	return choice;
}

CardInterface * BangPlayer::getBestPlay()
{
	State* state = dynamic_cast<State*>(getBestChoice());
	if (state) {
		if (state->getGCost() < playabilityTreshold)
			return nullptr;
		CardInterface* play = state->getPlay();
		if (play) {
			auto cardToRemove = std::find(hand.begin(), hand.end(), play);
			if(cardToRemove!=hand.end())
				hand.erase(cardToRemove);
		}
		return play;
	}
	return nullptr;
}

CardInterface * BangPlayer::getCurrentBestPlay()
{
	State* state = dynamic_cast<State*>(getCurrentBestChoice());
	CardInterface* play = state->getPlay();
	if (play) {
		auto cardToRemove = std::find(hand.begin(), hand.end(), play);
		if (cardToRemove != hand.end())
			hand.erase(cardToRemove);
	}
	return play;
}

void BangPlayer::evaluateCards()
{
	for (CardInterface* card : hand)
		evaluateCard(card);
}

void BangPlayer::addToTotalCost(double cost)
{
	totalHandValue += cost;
}

double BangPlayer::getAverageHandValue() const
{
	return averageHandValue;
}

void BangPlayer::calculateAverageHandValue()
{
	averageHandValue = totalHandValue / hand.size();
}

StateInterface * BangPlayer::getCurrentState() const
{
	return currentState;
}

StateInterface * BangPlayer::getCurrentBestChoice()
{
	StateInterface* state = nullptr;
	if (possibleChoices.size()) {
		state = possibleChoices.top();
		possibleChoices.pop();
	}
	return state;
}
