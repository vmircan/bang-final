#include "Indians.h"

Indians::Indians(const std::string & name, CardColor color, uint8_t number) :
	Card(name, color, number, CardType::BrownBorderedCard, BangCard::Indians, CardTargetType::Untargeted) {}

void Indians::playCard(Player * player, PlayerIteratorRange & range)
{
	state = CardState::Played;
	std::cout << player->getPlayerName() << " has played " << name << "\n";
	range.for_each([](Player* player) {
		if ((!player->isComputer() && !player->useBang()) ||
			(player->isComputer() && player->decideOnBangUse()))
			player->decreaseLifePoints(DamageSource::Indians);
	});
}

void Indians::filterTargetablePlayers(Player * player, PlayerIteratorRange & range)
{
}

void Indians::playCard(Player * player, Player * targetedPlayer)
{
}

bool Indians::canBePlayed(Player * player) const
{
	return true;
}
