#pragma once
#include <iterator>
template <typename T>
class CircularList
{
public:
	template <typename T>
	struct CircularListNode {
		CircularListNode(T* current) : current(current) {}
		bool operator==(CircularListNode<T> other) {
			return *current == *other.current;
		}
		T* current;
		CircularListNode<T> *left, *right;
	};
	class iterator : std::iterator<std::bidirectional_iterator_tag, CircularListNode<T>>  {
	public:
		iterator(CircularList<T>* list, CircularListNode<T>* current):
			list(list), current(current) {}
		~iterator(){}
		iterator(const iterator& it) : list(it.list), current(it.current) {}
		iterator& operator=(const iterator& it) {
			list = it.list;
			current = it.current;
			return *this;
		}
		bool operator==(const iterator& it) {
			return list == it.list && current == it.current;
		}
		bool operator!=(const iterator& it) {
			return !(*this == it);
		}
		iterator& operator+ (unsigned goForwardBy) {
			for (unsigned i = 0; i < goForwardBy; ++i, this->operator++())
				if (current == nullptr)
					return *this;
			return *this;
		}
		iterator& operator- (unsigned goBackBy) {
			for (unsigned i = 0; i < goBackBy; ++i, this->operator--())
				if (current == nullptr)
					return *this;
			return *this;
		}
		iterator& operator++() {
			current = current->right;
			if (*current == list->getHead())
				current = nullptr;
			return *this;
		}
		iterator& operator--() {
			current = current->left;
			if (*current == list->getHead())
				current = nullptr;
			return *this;
		}
		iterator operator++(int) {
			iterator temp(*this);
			++temp;
			return temp;
		}
		iterator operator--(int) {
			iterator temp(*this);
			--temp;
			return temp;
		}
		CircularListNode<T>* operator*() {
			return current;
		}
		void clear() {
			current = nullptr;
			list = nullptr;
		}
	private:
		CircularListNode<T>* current;
		CircularList<T>* list;
	};
	class reverse_iterator : std::iterator<std::bidirectional_iterator_tag, CircularListNode<T>> {
	public:
		reverse_iterator(CircularList<T>* list, CircularListNode<T>* current) :
			list(list), current(current) {}
		~reverse_iterator() {}
		reverse_iterator(const reverse_iterator& it) : list(it.list), current(it.current) {}
		reverse_iterator& operator=(const reverse_iterator& it) {
			list = it.list;
			current = it.current;
			return *this;
		}
		bool operator==(const reverse_iterator& it) {
			return list == it.list && current == it.current;
		}
		bool operator!=(const reverse_iterator& it) {
			return !(*this == it);
		}
		iterator& operator+ (unsigned goForwardBy) {
			for (unsigned i = 0; i < goForwardBy; ++i, this->operator++())
				if (current == nullptr)
					return *this;
			return *this;
		}
		iterator& operator- (unsigned goBackBy) {
			for (unsigned i = 0; i < goBackBy; ++i, this->operator--())
				if (current == nullptr)
					return *this;
			return *this;
		}
		reverse_iterator& operator++() {
			current = current->left;
			if (*current == list->getTail())
				current = nullptr;
			return *this;
		}
		reverse_iterator& operator--() {
			current = current->right;
			if (*current == list->getTail())
				current = nullptr;
			return *this;
		}
		reverse_iterator operator++(int) {
			reverse_iterator temp(*this);
			++temp;
			return temp;
		}
		reverse_iterator operator--(int) {
			reverse_iterator temp(*this);
			--temp;
			return temp;
		}
		CircularListNode<T>* operator*() {
			return current;
		}
		void clear() {
			current = nullptr;
			list = nullptr;
		}
	private:
		CircularListNode<T>* current;
		CircularList<T>* list;
	};


public:
	CircularList() : head(nullptr), tail(nullptr), listSize(0) {}
	~CircularList(){
		if (this->size()) {
			CircularListNode<T>* temp(head->right), *temp2;
			while (temp!=head)
			{
				temp2 = temp;
				temp = temp->right;
				delete temp2;
			}
			delete temp;
		}
	}
	iterator begin() {
		if (listSize)
			return iterator(this, head);
		else
			return end();
	}
	iterator end() {
		return iterator(this, nullptr);
	}
	reverse_iterator rbegin(){
		if (listSize)
			return reverse_iterator(this, tail);
		else
			return rend();
	}
	reverse_iterator rend(){
		return reverse_iterator(this, nullptr);
	}
	unsigned size() const{
		return listSize;
	}
	void add(T* element) {
		CircularListNode<T>* current = new CircularListNode<T>(element);
		if (!listSize)
			head = current;
		else
			tail->right = current;
		current->left = tail;
		current->right = head;
		tail = current;
		head->left = tail;
		++listSize;
	}
	void remove(unsigned elementNumber) {
		if (elementNumber > listSize - 1) {
			throw "Index number is too large";
			return;
		}
		CircularListNode<T>* temp;
		if (listSize == 1)
			delete head;
		else if (!elementNumber) {
			temp = head;
			head = head->right;
			tail->right = head;
			head->left = tail;
			delete temp;
		}
		else if (elementNumber == listSize - 1) {
			temp = tail;
			tail = tail->left;
			head->left = tail;
			tail->right = head;
			delete temp;
		}
		else {
			CircularListNode<T>* elementToDelete;
			temp = head;
			for (size_t i = 0; i < elementNumber - 1; ++i)
				temp = temp->right;
			elementToDelete = temp->right;
			temp->right = temp->right->right;
			temp->right->left = temp;
			delete elementToDelete;
		}
		--listSize;
	}
	CircularListNode<T> getHead() {
		return *head;
	}
	CircularListNode<T> getTail() {
		return *tail;
	}
private:
	CircularListNode<T>* head;
	CircularListNode<T>* tail;
	unsigned listSize;
};

